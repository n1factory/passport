/**
 * Reload & save documents editables
 * [PAssed from child iframe via event]
 */
window.addEventListener('document_save_reload', function(event) {
    var documentId = event.detail.document_id;

    var toolbarSelector = '#document_toolbar_' + documentId + '-innerCt .pimcore_save_button';
    var sidebarSelector = '#document_content_' + documentId + ' .x-toolbar-item';

    var saveButton = $(toolbarSelector).find('span[data-ref="btnEl"]').eq(1)
    var refreshButton = $(sidebarSelector).find('span[data-ref="btnEl"]').eq(1)

    if (saveButton.length > 0) {
        saveButton.click()
    }

    if (refreshButton.length > 0) {
        refreshButton.click()
    }
});
