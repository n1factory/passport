<?php 

return [
    6 => [
        "id" => 6,
        "name" => "Content Page",
        "group" => "My N1",
        "module" => "LayoutBundle",
        "controller" => "@LayoutBundle\\Controller\\LayoutController",
        "action" => "default",
        "template" => NULL,
        "type" => "page",
        "priority" => 0,
        "creationDate" => 1579594623,
        "modificationDate" => 1597841384
    ],
    7 => [
        "id" => 7,
        "name" => "Tip Document",
        "group" => "Tips",
        "module" => "ZebraTipsBundle",
        "controller" => "@ZebraTipsBundle\\Controller\\TipController",
        "action" => "template",
        "template" => NULL,
        "type" => "page",
        "priority" => 0,
        "creationDate" => 1583489464,
        "modificationDate" => 1583489464
    ],
    8 => [
        "id" => 8,
        "name" => "Tip Brick Document",
        "group" => "Tips",
        "module" => "ZebraTipsBundle",
        "controller" => "@ZebraTipsBundle\\Controller\\TipController",
        "action" => "tipBrickDocument",
        "template" => NULL,
        "type" => "page",
        "priority" => 0,
        "creationDate" => 1583489464,
        "modificationDate" => 1583489464
    ]
];
