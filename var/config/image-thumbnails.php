<?php 

return [
    "content" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 870,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "content",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 95,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1579165063,
        "creationDate" => 1579165063,
        "id" => "content"
    ],
    "galleryCarousel" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => "1140",
                    "height" => "400",
                    "positioning" => "center",
                    "doNotScaleUp" => "1"
                ]
            ]
        ],
        "medias" => [
            "940w" => [
                [
                    "method" => "cover",
                    "arguments" => [
                        "width" => "940",
                        "height" => "350",
                        "positioning" => "center"
                    ]
                ]
            ],
            "720w" => [
                [
                    "method" => "cover",
                    "arguments" => [
                        "width" => "720",
                        "height" => "300",
                        "positioning" => "center"
                    ]
                ]
            ],
            "320w" => [
                [
                    "method" => "cover",
                    "arguments" => [
                        "width" => "320",
                        "height" => "100",
                        "positioning" => "center"
                    ]
                ]
            ]
        ],
        "description" => "",
        "format" => "SOURCE",
        "quality" => 90,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "galleryCarousel"
    ],
    "galleryThumbnail" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => "260",
                    "height" => "180",
                    "positioning" => "center",
                    "doNotScaleUp" => "1"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "SOURCE",
        "quality" => 90,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "galleryThumbnail"
    ],
    "galleryCarouselPreview" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => "100",
                    "height" => "54",
                    "positioning" => "center",
                    "doNotScaleUp" => "1"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "SOURCE",
        "quality" => 90,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "galleryCarouselPreview"
    ],
    "galleryLightbox" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => "900"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "SOURCE",
        "quality" => 75,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "galleryLightbox"
    ],
    "productDetail" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 600,
                    "height" => 600
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "productDetail",
        "description" => "",
        "format" => "SOURCE",
        "quality" => 100,
        "highResolution" => 0,
        "modificationDate" => 1460099856,
        "creationDate" => 1460099856,
        "id" => "productDetail"
    ],
    "productList" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 310,
                    "height" => 310
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "productList",
        "description" => "",
        "format" => "SOURCE",
        "quality" => 75,
        "highResolution" => 0,
        "modificationDate" => 1459954835,
        "creationDate" => 1459936048,
        "id" => "productList"
    ],
    "shopDetail" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => "260",
                    "height" => "260"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "PNG",
        "quality" => 80,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "shopDetail"
    ],
    "shopDetailTechnology" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => "56",
                    "height" => "56"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "PNG",
        "quality" => 90,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "shopDetailTechnology"
    ],
    "shopDetailThumb" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 56,
                    "height" => 56,
                    "positioning" => "center",
                    "doNotScaleUp" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "shopDetailThumb",
        "description" => "",
        "format" => "SOURCE",
        "quality" => 80,
        "highResolution" => 0,
        "modificationDate" => 1460196959,
        "creationDate" => 1460196959,
        "id" => "shopDetailThumb"
    ],
    "print_keyvisual" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 2000
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_keyvisual",
        "description" => "",
        "format" => "PRINT",
        "quality" => 90,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1481124999,
        "creationDate" => 1481124976,
        "id" => "print_keyvisual"
    ],
    "print_productcell" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 600,
                    "height" => 600
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_productcell",
        "description" => "",
        "format" => "PRINT",
        "quality" => 99,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1481729573,
        "creationDate" => 1481278050,
        "id" => "print_productcell"
    ],
    "print_productcell_small" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 200,
                    "height" => 200
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_productcell_small",
        "description" => "",
        "format" => "PRINT",
        "quality" => 90,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1481278789,
        "creationDate" => 1481278780,
        "id" => "print_productcell_small"
    ],
    "print_titleimage" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 1000
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_titleimage",
        "description" => "",
        "format" => "PRINT",
        "quality" => 90,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1482149676,
        "creationDate" => 1481298467,
        "id" => "print_titleimage"
    ],
    "print_backgroundimage" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 3000
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_backgroundimage",
        "description" => "",
        "format" => "PRINT",
        "quality" => 90,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1481530257,
        "creationDate" => 1481530238,
        "id" => "print_backgroundimage"
    ],
    "profilePageImage" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => 100,
                    "height" => 100
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "profilePageImage",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1558700920,
        "creationDate" => 1558699556,
        "id" => "profilePageImage"
    ],
    "headerAvatar" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => 28,
                    "height" => 28
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "headerAvatar",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1558702832,
        "creationDate" => 1558701009,
        "id" => "headerAvatar"
    ],
    "userTableImage" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => 24,
                    "height" => 24
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "userTableImage",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1558703179,
        "creationDate" => 1558703171,
        "id" => "userTableImage"
    ],
    "tip_image" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 700,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "tip_image",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1579166487,
        "creationDate" => 1579164186,
        "id" => "tip_image"
    ],
    "dashboard_card" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 682,
                    "height" => 173,
                    "positioning" => "center",
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "dashboard_card",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1585824966,
        "creationDate" => 1585824873,
        "id" => "dashboard_card"
    ]
];
