Create a package:
* php create_package.php [version] [source-application-path] (e.g. php create_package.php 1.0.0.1 ~/web/n1/pimcore/)

This will create a package named n1_release_1.0.0.tar.gz.

Extract a package:
* php extract_package.php [archive-file] [source-application-path] (e.g. php extract_package.php n1_release_1.0.0.tar.gz target_dir/)

This will extract the archive to the given directory.

For a proper release there also has to be created a file like install_1.0.0.1.txt to perform additional (manual) tasks! See also example
file build/releases/install_1.0.0.1.txt.






