<?php

$excludePaths = [
    "build",
    "web/var",
    "var/cache",
    "var/email",
    "var/logs",
    "var/recyclebin",
    "var/sessions",
    "var/tmp",
    "var/versions",
    "var/config/system.php",
    "app/config/local/database.yml",
    ".git",
    "package.json",
    "nbproject",
    "package-lock.json",
    "composer.json",
    "composer.lock",
    ".gitignore",
    ".gitattributes",
    ".env.example"
];

function println($string)
{
    echo PHP_EOL . $string . PHP_EOL;
}

$usageHint = "Usage: php create_package.php [version] [source-application-path]";

if(!isset($argv[1])) {
    println("A version is required! $usageHint");
    die;
}

if(!isset($argv[2])) {
    println("A source dir is required! $usageHint");
    die;
}

$version = $argv[1];

$sourceDir = $argv[2];

if(!is_dir($sourceDir)) {
    println("Source dir $sourceDir does not exist! $usageHint");
    die;    
}

$sourceDirPath = rtrim($sourceDir,"/");

$cacheDir = $sourceDirPath . "/var/cache";

if(!is_dir($cacheDir)) {
    println("Cache dir $sourceDir does not exist!");
    die;       
}

$clearCacheCommand = "rm -rf $cacheDir/*";

exec($clearCacheCommand);

$explodedSourceDir = explode("/", $sourceDirPath);

if($explodedSourceDir[0] === "") {
    unset($explodedSourceDir[0]);
    $explodedSourceDir = array_values($explodedSourceDir);
}

$targetDir = array_pop($explodedSourceDir);

$sourceDirPath = "/" . implode("/", $explodedSourceDir);

if($sourceDirPath === "" || $sourceDirPath === "/") {
    $sourceDirPath = ".";
}

$tarFilename = "bag_release_$version.tar.gz";

$excludePathsAsString = "";
foreach ($excludePaths as $excludePath) {
    $targetExludePath = "$targetDir/" . $excludePath;
    
    $excludePathsAsString .= " --exclude=$targetExludePath";
}

println("Create archive $tarFilename");

$command = "tar -chzf $tarFilename $excludePathsAsString -C $sourceDirPath $targetDir";

exec($command);



