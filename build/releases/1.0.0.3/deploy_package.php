<?php

function println($string)
{
    echo PHP_EOL . $string . PHP_EOL;
}

$usageHint = "Usage: php deploy_package.php [archive-file] [target-application-path] [target-owner] [target-group]

* archive-file: The .tar.gz file to deploy
* target-application-path: Path to application root (not document-root!)
* target-owner: Owner for unix file/directory permissions
* target-group: Group for unix file/directory permissions
        ";

if(!isset($argv[1])) {
    println("A source file is required! $usageHint");
    die;
}

if(!isset($argv[2])) {
    println("A target dir is required! $usageHint");
    die;
}

if(!isset($argv[3])) {
    println("A owner is required! $usageHint");
    die;
}

if(!isset($argv[4])) {
    println("A group dir is required! $usageHint");
    die;
}

$sourceFile = $argv[1];

if(!is_file($sourceFile)) {
    println("Source file $sourceFile does not exist! $usageHint");
    die;
}

$targetDir = $argv[2];

if(!is_dir($targetDir)) {
    println("Target dir $targetDir does not exist! $usageHint");
    die;
}

if(substr($targetDir, -1) !== "/") {
    $targetDir .= "/";
}

$owner = $argv[3];

$group = $argv[4];

$command = "tar -xf $sourceFile -C $targetDir/../tmp_folder --strip-components=1 --overwrite --warning=none";

println($command);

exec($command);

$chownerAndGroupCommand = "chown -R $owner:$group $targetDir/../tmp_folder";

println($chownerAndGroupCommand);

exec($chownerAndGroupCommand);

$cpCommand = "cp -r $targetDir/../tmp_folder/* $targetDir/.";

println($cpCommand);

exec($cpCommand);

exec("rm -rf $targetDir/../tmp_folder");

