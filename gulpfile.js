var gulp = require('gulp'),
        config = require('dotenv').config(),
        argv = require('yargs').argv,
        sass = require('gulp-sass'),
        minifyCSS = require('gulp-minify-css'),
        uglify = require('gulp-uglify'),
        plumber = require('gulp-plumber'),
        concat = require('gulp-concat'),
        webpack = require('webpack'),
        webpackStream = require('webpack-stream'),
        webpackConfig = require('./webpack.config.js'),
        autoprefixer = require('gulp-autoprefixer'),
        browserSync = require('browser-sync').create();

gulp.task('scripts', function () {
    gulp.src([
        'src/UserAuthenticationBundle/Resources/public/js/*.js',
        'src/Factory/UserManagementBundle/Resources/public/js/*.js',
        'src/Factory/ContractManagementBundle/Resources/public/js/*.js',
        'src/Factory/DashboardBundle/Resources/public/js/**',
        '!src/Factory/DispositionBundle/Resources/public/js/pimcore/**',
        'src/Factory/DispositionBundle/Resources/public/js/**',
        // 'src/Factory/DispositionBundle/Resources/public/js/calloff-companies.js',
        '!src/Factory/DashboardBundle/Resources/public/js/pimcore/**',
        'src/AppBundle/Resources/public/js/*.js',
        'src/LayoutBundle/Resources/public/js/*.js',
        'src/AreabrickBundle/Resources/public/js/*.js',
        'src/ContactFormBundle/Resources/public/js/*.js',
        'src/PassportBundle/Resources/public/js/*.js',
        '!src/PassportBundle/Resources/public/js/pimcore/**',
    ])
            .pipe(plumber())
            .pipe(webpackStream(webpackConfig), webpack)
            .pipe(concat('custom.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest('web/static/js/'))
            .pipe(browserSync.stream())
            .on('change', browserSync.reload);
});

gulp.task('n1-dashboard-scripts', function () {
    gulp.src([
        'src/NOneDashboardBundle/Resources/public/js/*.js'
    ])
            .pipe(plumber())
            .pipe(webpackStream(webpackConfig), webpack)
            .pipe(concat('n1-dashboard.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest('web/static/js/'))
            .pipe(browserSync.stream())
            .on('change', browserSync.reload);
});

// gulp.task('vue-scripts', function () {
//     gulp.src([
//         '!src/Factory/DispositionBundle/Resources/public/js/calloff-companies.js',
//         'src/Factory/DispositionBundle/Resources/public/js/vuejs/**',

//     ])
//         // .pipe(plumber())
//         .pipe(webpackStream(webpackConfig), webpack)
//         .pipe(concat('vue-custom.min.js'))
//         .pipe(uglify())
//         .pipe(gulp.dest('web/static/js/'))
//         .pipe(browserSync.stream())
//         .on('change', browserSync.reload);
// });

gulp.task('tip-scripts', function () {
    gulp.src([
        'src/ZebraTipsBundle/Resources/public/js/**',
        '!src/ZebraTipsBundle/Resources/public/js/pimcore/**',

    ])
            .pipe(plumber())
            .pipe(webpackStream(webpackConfig), webpack)
            .pipe(concat('tips-custom.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest('web/static/js/'))
            .pipe(browserSync.stream())
            .on('change', browserSync.reload);
});

gulp.task('sass', function () {
    gulp.src([
        'src/UserAuthenticationBundle/Resources/public/sass/main.sass',
        'src/Factory/UserManagementBundle/Resources/public/sass/main.sass',
        'src/Factory/ContractManagementBundle/Resources/public/sass/main.sass',
        'src/Factory/DashboardBundle/Resources/public/sass/main.sass',
        'src/Factory/DispositionBundle/Resources/public/sass/main.sass',
        'src/AppBundle/Resources/public/sass/main.sass',
        'src/LayoutBundle/Resources/public/sass/main.sass',
        'src/AreabrickBundle/Resources/public/sass/main.sass',
        'src/ContactFormBundle/Resources/public/sass/main.sass',
        'src/PassportBundle/Resources/public/sass/main.sass',
    ])
            .pipe(sass.sync().on('error', sass.logError))
            .pipe(autoprefixer({browsers: ['last 30 versions']}))
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(concat('custom.css'))
            .pipe(minifyCSS())
            .pipe(gulp.dest('web/static/css/'))
            .pipe(browserSync.stream());
});

gulp.task('tip-sass', function () {
    gulp.src([
        'src/ZebraTipsBundle/Resources/public/sass/main.sass',
        'ZebraTipsBundle/Resources/public/js'
    ])
            .pipe(sass.sync().on('error', sass.logError))
            .pipe(autoprefixer({browsers: ['last 30 versions']}))
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(concat('tip-custom.css'))
            .pipe(minifyCSS())
            .pipe(gulp.dest('web/static/css/'))
            .pipe(browserSync.stream());
});

gulp.task('n1-dashboard-sass', function () {
    gulp.src([
        'src/NOneDashboardBundle/Resources/public/sass/main.sass',
    ])
            .pipe(sass.sync().on('error', sass.logError))
            .pipe(autoprefixer({browsers: ['last 30 versions']}))
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(concat('n1-dashboard.min.css'))
            .pipe(minifyCSS())
            .pipe(gulp.dest('web/static/css/'))
            .pipe(browserSync.stream());
});

gulp.task('material', function () {
    gulp.src([
        'web/static/sass/material-dashboard.scss',
    ])
            .pipe(sass.sync().on('error', sass.logError))
            .pipe(autoprefixer({browsers: ['last 30 versions']}))
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(concat('material-dashboard.css'))
            .pipe(minifyCSS())
            .pipe(gulp.dest('web/static/css/'))
            .pipe(browserSync.reload({stream:true}));
});

gulp.task('php', function () {
    gulp.src([
        'app/Resources/views/**/*.php',
        'src/UserAuthenticationBundle/**/*.php',
        'src/Factory/UserManagementBundle/**/*.php',
        'src/Factory/ContractManagementBundle/**/*.php',
        'src/Factory/DashboardBundle/**/*.php',
        'src/Factory/DispositionBundle/**/*.php',
        'src/AppBundle/**/*.php',
        'src/LayoutBundle/**/*.php',
        'src/ContactFormBundle/**/*.php',
        'src/PassportBundle/**/*.php',
    ])
            .pipe(browserSync.stream())
            .on('change', browserSync.reload);
});

gulp.task('twig', function () {
    gulp.src([
        'app/Resources/views/**/*.html.twig',
        'src/UserAuthenticationBundle/**/*.html.twig',
        'src/Factory/UserManagementBundle/**/*.html.twig',
        'src/Factory/ContractManagementBundle/**/*.html.twig',
        'src/Factory/DashboardBundle/**/*.html.twig',
        'src/Factory/DispositionBundle/**/*.html.twig',
        'src/AppBundle/**/*.html.twig',
        'src/LayoutBundle/**/*.html.twig',
        'src/ContactFormBundle/**/*.html.twig',
        'src/PassportBundle/**/*.html.twig',
    ])
            .pipe(browserSync.stream())
            .on('change', browserSync.reload);
});
gulp.task('watch', function () {
    // Get proxying domain from .env file
    let proxy = process.env.DOMAIN
    if(argv.passport === true) proxy = process.env.DOMAIN_PASSPORT
    browserSync.init({
        proxy,
    });

    gulp.watch([
        'app/Resources/views/**/*.php',
        'src/UserAuthenticationBundle/**/*.php',
        'src/Factory/UserManagementBundle/**/*.php',
        'src/Factory/ContractManagementBundle/**/*.php',
        'src/Factory/DashboardBundle/**/*.php',
        'src/Factory/DispositionBundle/**/*.php',
        'src/AppBundle/**/*.php',
        'src/LayoutBundle/**/*.php',
        'src/AreabrickBundle/**/*.php',
        'src/ContactFormBundle/**/*.php',
        'src/PassportBundle/**/*.php',
    ], ['php']);

    gulp.watch([
        'app/Resources/views/**/*.html.twig',
        'src/UserAuthenticationBundle/**/*.html.twig',
        'src/Factory/UserManagementBundle/**/*.html.twig',
        'src/Factory/ContractManagementBundle/**/*.html.twig',
        'src/Factory/DashboardBundle/**/*.html.twig',
        'src/Factory/DispositionBundle/**/*.html.twig',
        'src/AppBundle/**/*.html.twig',
        'src/LayoutBundle/**/*.html.twig',
        'src/AreabrickBundle/**/*.html.twig',
        'src/ContactFormBundle/**/*.html.twig',
        'src/PassportBundle/**/*.html.twig',
    ], ['twig']);

    gulp.watch([
        'web/static/sass/**',
    ], ['material']);

    gulp.watch([
        'src/UserAuthenticationBundle/Resources/public/sass/**',
        'src/Factory/UserManagementBundle/Resources/public/sass/**',
        'src/Factory/ContractManagementBundle/Resources/public/sass/**',
        'src/Factory/DashboardBundle/Resources/public/sass/**',
        'src/Factory/DispositionBundle/Resources/public/sass/**/**',
        'src/AppBundle/Resources/public/sass/**',
        'src/LayoutBundle/Resources/public/sass/**',
        'src/AreabrickBundle/Resources/public/sass/**',
        'src/ContactFormBundle/Resources/public/sass/**',
        'src/PassportBundle/Resources/public/sass/**',
    ], ['sass']);

    gulp.watch([
        'src/UserAuthenticationBundle/Resources/public/js/**',
        'src/Factory/UserManagementBundle/Resources/public/js/**',
        'src/Factory/ContractManagementBundle/Resources/public/js/**',
        'src/Factory/DashboardBundle/Resources/public/js/**',
        'src/Factory/DispositionBundle/Resources/public/js/**',
        // '!src/Factory/DispositionBundle/Resources/public/js/vuejs/**',
        'src/AppBundle/Resources/public/js/**',
        'src/LayoutBundle/Resources/public/js/**',
        'src/AreabrickBundle/Resources/public/js/**',
        'src/ContactFormBundle/Resources/public/js/**',
        'src/PassportBundle/Resources/public/js/**',
    ], ['scripts']);

    // gulp.watch([
    //     '!src/Factory/DispositionBundle/Resources/public/js/calloff-companies.js',
    //     'src/Factory/DispositionBundle/Resources/public/js/vuejs/**',
    // ], ['vue-scripts']);
    gulp.watch([
        'src/ZebraTipsBundle/Resources/public/js/**',
        '!src/ZebraTipsBundle/Resources/public/js/pimcore/**'
    ], ['tip-scripts']);
    gulp.watch([
        'src/ZebraTipsBundle/Resources/public/sass/**',
    ], ['tip-sass']);

    gulp.watch([
        'src/NOneDashboardBundle/Resources/public/js/**',
    ], ['n1-dashboard-scripts']);
    gulp.watch([
        'src/NOneDashboardBundle/Resources/public/sass/**',
    ], ['n1-dashboard-sass']);
});

// Default "watch" task for development
gulp.task('default', ['watch']);

// "Build" task for assets
gulp.task('build', ['sass', 'scripts', 'tip-sass', 'tip-scripts', 'twig', 'n1-dashboard-scripts', 'n1-dashboard-sass']);