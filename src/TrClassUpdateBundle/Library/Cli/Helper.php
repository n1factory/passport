<?php

namespace TrClassUpdateBundle\Library\Cli;

class Helper {
    
    const OUTPUT_INFO = "info";
    
    const OUTPUT_ERROR = "error";
    
    const OUTPUT_SUCCESS = "success";
    
    const OUTPUT_SECTION = "section";
    
    const OUTPUT_ALL = "all";
    
    /**
     * If set to false, echoing output is disabled
     * @var bool 
     */
    public static $echoOutput = true;
    
    /**
     *
     * @var array 
     */
    private static $outputBuffer = [
        "info" => [],
        "error" => [],
        "success" => [],
        "section" => []
    ];

    /**
     * Default ouput color
     * @param string $message
     */
    public static function info(string $message) {
        self::$outputBuffer[self::OUTPUT_INFO][] = "INFO: " . $message;
        
        self::echoOutput($message);
    }
    
    /**
     * Red color output
     * @param string $message
     */
    public static function error(string $message) {
        self::$outputBuffer[self::OUTPUT_ERROR][] = "ERROR: " . $message;
        
        self::echoOutput("\033[31m{$message}\033[0m \e[m");
    }

    /**
     * Green color output
     * @param string $message
     */
    public static function success(string $message) {
        self::$outputBuffer[self::OUTPUT_SUCCESS][] = "SUCCESS: " . $message;
        
        self::echoOutput("\033[32m{$message}\033[0m \e[m");
    }
    
    /**
     * Green color output
     * @param string $message
     */
    public static function section(string $message) {
        self::$outputBuffer[self::OUTPUT_SECTION][] = "SECTION: " . $message;
        
        self::echoOutput("\033[34m{$message}\033[1m \e[m");
    }   
    
    /**
     * 
     * @param string $string
     */
    private static function echoOutput(string $string) {
        if(self::$echoOutput === true) {
            echo PHP_EOL . $string . PHP_EOL;
        }
    }
    
    /**
     * 
     * @param boolean $asArray
     * @return array|string
     */
    public static function getOuputBuffer($asArray = false, $type = "all") {
        if($asArray === true && $type === self::OUTPUT_ALL) {
            return self::$outputBuffer;
        }
        
        if($asArray === true && $type !== self:: OUTPUT_ALL) {
            return self::$outputBuffer[$type];
        }        
        
        $outputBuffer = self::$outputBuffer;
        
        if($type !== self::OUTPUT_ALL) {
            $outputBuffer = [];
            
            $outputBuffer[$type] = self::$outputBuffer[$type];
        } 
        
        $output = "";
        foreach ($outputBuffer as $key => $lines) {
            $output .= PHP_EOL;
            
            if(count($lines) > 0) {
                $output .= implode(PHP_EOL, $lines);
            }
            
            $output .= PHP_EOL;
        }

        $output = preg_replace('/^[ \t]*[\r\n]+/m', '', $output);
        
        return $output;
    }
    
    /**
     * 
     * @param string $type
     * @param string $filepath
     */
    public static function saveFile ($filepath = "output_buffer.log", $type = "all") {
        $data = self::getOuputBuffer(false, $type);
        
        file_put_contents($filepath, $data);
    }
}
