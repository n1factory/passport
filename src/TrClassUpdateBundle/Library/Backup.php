<?php

namespace TrClassUpdateBundle\Library;

use TrClassUpdateBundle\Library\Helper;

class Backup
{
    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function backup()
    {
        Cli\Helper::section("BACKUP: Delete files from temp dir");
        $this->cleanupTempDirFiles();       
        
        Cli\Helper::section("BACKUP: Delete backup files older than " . $this->config->getDumpKeepDays() . " days");
        $this->cleanupOldFiles();

        Cli\Helper::section("BACKUP: Create database backup in directory " . Helper::getBackupPath());
        $databaseFile = $this->backupDatabase();
        
        Cli\Helper::section("BACKUP: Create classes backup in directory " . Helper::getBackupPath());
        $classesFile = $this->backupClasses();
        
        $targetPath = Helper::getBackupPath() . "/" . date("Y-m-d_H_i_s") . ".tar.gz";
        
        $tarCommand = "tar -cPzf $targetPath $databaseFile $classesFile";
        
        @exec($tarCommand);
        
        unlink($databaseFile);
        unlink($classesFile);
    }

    public function backupDatabase()
    {
        $dbConfig = \Pimcore\Config::getSystemConfig()->toArray()["database"]["params"];

        $dumpTargetPath = Helper::getTempPath() . "/" . date("Y-m-d_H_i_s") . "_{$dbConfig["dbname"]}.sql";

        $mysqlCommand = "mysqldump -u'{$dbConfig["username"]}' -p'{$dbConfig["password"]}' -h'{$dbConfig["host"]}' {$dbConfig["dbname"]} > $dumpTargetPath 2>&1 | grep -v \"Warning: Using a password\"";

        @exec($mysqlCommand, $output, $return);    
        
        return $dumpTargetPath;
    }
    
    public function backupClasses()
    {
        $sourcePath = Helper::getClassesPath();
        
        $targetPath = Helper::getTempPath() . "/" . date("Y-m-d_H_i_s") . "_classes" . ".tar";
        
        $classesCommand = "tar -cPf $targetPath $sourcePath";
        
        @exec($classesCommand); 
        
        return $targetPath;
    }
    
    private function cleanupTempDirFiles()
    {
        $files = Helper::getFilesFromDirectory(Helper::getTempPath());
        
        foreach ($files as $file) {
            $filepath = Helper::getTempPath() . "/" . $file;
            
            if (is_file($filepath) === true) {
                unlink($filepath);
            }
        }        
    }
    
    private function cleanupOldFiles()
    {
        $files = Helper::getFilesFromDirectory(Helper::getBackupPath());
        
        $targetTimestamp = 60 * 60 * 24 * $this->config->getDumpKeepDays();
        
        $now = time();
        foreach ($files as $file) {
            $filepath = Helper::getBackupPath() . "/" . $file;
            
            if (is_file($filepath) === true && $now - filemtime($filepath) >= $targetTimestamp) {
                unlink($filepath);
            }
        }
    }
}
