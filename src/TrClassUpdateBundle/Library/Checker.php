<?php

namespace TrClassUpdateBundle\Library;

use TrClassUpdateBundle\Library\Export;
use TrClassUpdateBundle\Library\Import;
use TrClassUpdateBundle\Library\Config;
use TrClassUpdateBundle\Library\Cli\Helper as CliHelper;

class Checker
{
    protected $config;
    protected $import;
    protected $export;

    public function __construct(Config $config, Import $import, Export $export)
    {
        $this->config = $config;

        $this->import = $import;

        $this->export = $export;
    }

    public function check(): array
    {
        $importSchemes = $this->getImportSchemes();

        $exportSchemes = $this->getExportSchemes();

        $check = [];
        foreach ($importSchemes as $schemeType => $schemeData) {
            foreach ($schemeData as $schemeTypeKey => $schemeTypeData) {
                $flag = $this->diff($schemeTypeData, $exportSchemes[$schemeType][$schemeTypeKey]);

                if ($flag === false) {
                    $outputType = ucfirst($schemeType);

                    $message = "{$outputType}: {$schemeTypeKey} has changed - please check";

                    $check[] = $message;

                    CliHelper::error($message);
                }
            }
        }

        return $check;
    }

    public function getImportSchemes(): array
    {
        $data = [];

        $data[Config::SCHEMA_TYPE_CLASSES] = $this->import->importClasses(true, true);

        $data[Config::SCHEMA_TYPE_CUSTOM_LAYOUTS] = $this->import->importCustomLayouts(true, true);

        $data[Config::SCHEMA_TYPE_FIELDCOLLECTIONS] = $this->import->importFieldCollections(true, true);

        $data[Config::SCHEMA_TYPE_OBJECTBRICKS] = $this->import->importObjectBricks(true, true);

        return $data;
    }

    public function diff($array1, $array2): bool
    {
        return $array1 === $array2;
    }

    public function getExportSchemes(): array
    {
        $data = [];

        $data[Config::SCHEMA_TYPE_CLASSES] = $this->export->exportClasses(null, true);

        $data[Config::SCHEMA_TYPE_CUSTOM_LAYOUTS] = $this->export->exportCustomLayouts(null, true);

        $data[Config::SCHEMA_TYPE_FIELDCOLLECTIONS] = $this->export->exportFieldcollections(null, true);

        $data[Config::SCHEMA_TYPE_OBJECTBRICKS] = $this->export->exportObjectBricks(null, true);

        return $data;
    }
}
