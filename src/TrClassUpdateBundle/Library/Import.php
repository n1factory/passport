<?php

namespace TrClassUpdateBundle\Library;

use Pimcore\Model\DataObject;
use TrClassUpdateBundle\Library\Config;
use TrClassUpdateBundle\Library\Helper;
use Pimcore\Model\DataObject\ClassDefinition\Service as ClassDefinitionService;
use TrClassUpdateBundle\Library\Cli\Helper as CliHelper;

class Import
{
    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function import()
    {
        CliHelper::section("CUSTOM LAYOUTS IMPORT");
        $this->importCustomLayouts();

        CliHelper::section("CLASSES IMPORT");
        $this->importClasses();

        CliHelper::section("FIELDCOLLECTIONS IMPORT");
        $this->importFieldCollections();

        CliHelper::section("OBJECTBRICKS IMPORT");
        $this->importObjectBricks();
    }

    public function importCustomLayouts($returnOnly = false, $asArray = false)
    {
        $files = Helper::getFilesFromDirectory(Helper::getSchemaPath(Config::SCHEMA_TYPE_CUSTOM_LAYOUTS));

        $fileData = $this->getDataFromFiles($files, Helper::getSchemaPath(Config::SCHEMA_TYPE_CUSTOM_LAYOUTS), Config::FILE_TYPE_CUSTOM_LAYOUT, $asArray);

        if ($returnOnly === true) {
            return $fileData;
        }

        foreach ($fileData as $customDefinitionKey => $jsonCustomLayoutData) {
            $customLayoutData = json_decode($jsonCustomLayoutData, true);

            $id = $customLayoutData["id"];
            unset($customLayoutData["id"]);

            $name = $customLayoutData["name"];
            unset($customLayoutData["name"]);

            $classId = $customLayoutData["classId"];
            unset($customLayoutData["classId"]);

            $rawCustomLayout = \Pimcore\Db::get()->fetchRow('SELECT * FROM custom_layouts WHERE id = ?', $id);

            if (!$rawCustomLayout) {
                $customLayout = DataObject\ClassDefinition\CustomLayout::create(
                                [
                                    'name' => $name,
                                    'userOwner' => $this->config->getUpdateUserId(),
                                    'classId' => $classId
                                ]
                );

                $customLayout->setId($id);
                $customLayout->save();

                CliHelper::success("CUSTOM LAYOUT create: {$name}($id)");
            } else {
                CliHelper::info("CUSTOM LAYOUT update: {$name}($id)");

                $customLayout = DataObject\ClassDefinition\CustomLayout::getById($id);
            }

            if ($customLayoutData['layoutDefinitions']) {
                $layout = DataObject\ClassDefinition\Service::generateLayoutTreeFromArray($customLayoutData['layoutDefinitions'], true);
                $customLayout->setLayoutDefinitions($layout);
            }

            $customLayout->setDescription($customLayoutData['description']);
            $customLayout->save();
        }
    }

    public function importClasses($returnOnly = false, $asArray = false)
    {
        $files = Helper::getFilesFromDirectory(Helper::getSchemaPath(Config::SCHEMA_TYPE_CLASSES));

        $fileData = $this->getDataFromFiles($files, Helper::getSchemaPath(Config::SCHEMA_TYPE_CLASSES), Config::FILE_TYPE_CLASS, $asArray);

        if ($returnOnly === true) {
            return $fileData;
        }

        foreach ($fileData as $className => $jsonClassData) {
            $classDefiniton = \Pimcore\Model\DataObject\ClassDefinition::getByName($className);

            if ($classDefiniton === null) {
                $classData = json_decode($jsonClassData, true);

                $className = Helper::normalizeFilename($className);

                $userId = $this->config->getUpdateUserId();

                $classDefiniton = \Pimcore\Model\DataObject\ClassDefinition::create(['name' => $className, 'userOwner' => $userId, 'id' => $classData['id']]);

                $classDefiniton->save();

                CliHelper::success("CLASS create: " . $className);
            } else {
                CliHelper::info("CLASS update: " . $className);
            }

            ClassDefinitionService::importClassDefinitionFromJson($classDefiniton, $jsonClassData);
        }
    }

    public function importFieldCollections($returnOnly = false, $asArray = false)
    {
        $files = Helper::getFilesFromDirectory(Helper::getSchemaPath(Config::SCHEMA_TYPE_FIELDCOLLECTIONS));

        $fileData = $this->getDataFromFiles($files, Helper::getSchemaPath(Config::SCHEMA_TYPE_FIELDCOLLECTIONS), Config::FILE_TYPE_FIELDCOLLECTION, $asArray);

        if ($returnOnly === true) {
            return $fileData;
        }

        foreach ($fileData as $fieldCollectionKey => $jsonFieldCollectionData) {
            $fieldCollectionDefiniton = null;
            try {
                $fieldCollectionDefiniton = \Pimcore\Model\DataObject\Fieldcollection\Definition::getByKey($fieldCollectionKey);
            } catch (\Exception $ex) {
                
            }

            if ($fieldCollectionDefiniton === null) {
                $fieldCollectionKey = Helper::normalizeFilename($fieldCollectionKey);

                $fieldCollectionDefiniton = new DataObject\Fieldcollection\Definition();

                $fieldCollectionDefiniton->setKey($fieldCollectionKey);

                $configuration = json_decode($jsonFieldCollectionData, true);

                $configuration['datatype'] = 'layout';
                $configuration['fieldtype'] = 'panel';

                $layout = DataObject\ClassDefinition\Service::generateLayoutTreeFromArray($configuration, true);

                $fieldCollectionDefiniton->setLayoutDefinitions($layout);

                $fieldCollectionDefiniton->save();

                CliHelper::success("FIELDCOLLECTION create: " . $fieldCollectionKey);
            } else {
                CliHelper::info("FIELDCOLLECTION update: " . $fieldCollectionKey);
            }

            ClassDefinitionService::importFieldCollectionFromJson($fieldCollectionDefiniton, $jsonFieldCollectionData);
        }
    }

    public function importObjectBricks($returnOnly = false, $asArray = false)
    {
        $files = Helper::getFilesFromDirectory(Helper::getSchemaPath(Config::SCHEMA_TYPE_OBJECTBRICKS));

        $fileData = $this->getDataFromFiles($files, Helper::getSchemaPath(Config::SCHEMA_TYPE_OBJECTBRICKS), Config::FILE_TYPE_FIELDCOLLECTION, $asArray);

        if ($returnOnly === true) {
            return $fileData;
        }

        foreach ($fileData as $objectbrickKey => $jsonObjectbrickData) {
            $objectbrickDefiniton = null;
            try {
                $objectbrickDefiniton = \Pimcore\Model\DataObject\Objectbrick\Definition::getByKey($objectbrickKey);
            } catch (\Exception $ex) {
                
            }

            if ($objectbrickDefiniton === null) {
                $objectbrickKey = Helper::normalizeFilename($objectbrickKey);

                $objectbrickDefiniton = new DataObject\Objectbrick\Definition();

                $objectbrickDefiniton->setKey($objectbrickKey);

                $configuration = json_decode($jsonObjectbrickData, true);

                $configuration['datatype'] = 'layout';
                $configuration['fieldtype'] = 'panel';

                $layout = DataObject\ClassDefinition\Service::generateLayoutTreeFromArray($configuration, true);

                $objectbrickDefiniton->setLayoutDefinitions($layout);

                $objectbrickDefiniton->save();

                CliHelper::success("OBJECTBRICK create: " . $objectbrickKey);
            } else {
                CliHelper::info("OBJECTBRICK update: " . $objectbrickKey);
            }

            ClassDefinitionService::importObjectBrickFromJson($objectbrickDefiniton, $jsonObjectbrickData);
        }
    }

    private function getDataFromFiles($files, $basepath, $context, $asArray = false): array
    {
        $return = [];
        foreach ($files as $filename) {
            $filecontent = file_get_contents($basepath . "/" . $filename);

            $className = str_replace([$context . "_", ".json"], "", $filename);

            $return[$className] = $asArray === false ? $filecontent : json_decode($filecontent, true);
        }

        return $return;
    }
}
