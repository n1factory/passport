pimcore.registerNS("pimcore.plugin.TrClassUpdateBundle");

pimcore.plugin.TrClassUpdateBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.TrClassUpdateBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("TrClassUpdateBundle ready!");
    }
});

var TrClassUpdateBundlePlugin = new pimcore.plugin.TrClassUpdateBundle();
