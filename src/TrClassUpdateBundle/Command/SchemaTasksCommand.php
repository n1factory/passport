<?php

namespace TrClassUpdateBundle\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Console\Dumper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use TrClassUpdateBundle\Library\Import;
use TrClassUpdateBundle\Library\Export;
use TrClassUpdateBundle\Library\Checker;
use TrClassUpdateBundle\Library\Config;
use TrClassUpdateBundle\Library\Cli\Helper as CliHelper;
use TrClassUpdateBundle\Library\Backup;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class SchemaTasksCommand extends AbstractCommand
{

    protected function configure()
    {
        $this->setName('tr:schema')->setDescription('Export/import/check of classes, bricks, customlayouts and fieldcollections')
                ->addOption(
                        'export', null, InputOption::VALUE_NONE, 'Export classes, bricks, customlayouts and fieldcollections')
                ->addOption(
                        'import', null, InputOption::VALUE_NONE, 'Import classes, bricks, customlayouts and fieldcollections')
                ->addOption(
                        'backup', null, InputOption::VALUE_NONE, 'Create backup only')                   
                ->addOption(
                        'no-backup', null, InputOption::VALUE_NONE, 'Do not create any backups (not recommended in live environment!)')
                ->addOption(
                        'check', null, InputOption::VALUE_NONE,
                        'Check classes, bricks and fieldcollections for changes incoming by git - is also made before every import')
        ->setHelp("Requirements:
* binaries on cli:
mysqldump
tar
* folder structure in application root:
schema
    backups (gitignore)
    classes
    customlayouts
    fieldcollections
    logs (gitignore)
    objectbricks
    tmp (gitignore)

Instructions:

Following values can be overridden in website settings:

tcub_dump_keep_days (int | default 10): days to keep backups before deleting
tcub_update_user_id (int | default 3): user to update with classes, etc.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $options = $input->getOptions();

        if ($options["quiet"] === true) {
            CliHelper::$echoOutput = false;
        }

        $result = [];
        foreach (["export", "import", "check", "backup"] as $key) {
            if ($options[$key]) {
                $result[] = $key;
            }
        }

        if (count($result) === 0 || count($result) > 1) {
            throw new \Exception("You have to use at least one option!");
        }

        $task = $result[0];

        $config = new Config();

        $import = new Import($config);

        $export = new Export($config);

        $checker = new Checker($config, $import, $export);
        
        if ($options["backup"] === true) {
            $backup = new Backup($config);

            $backup->backup();
            
            return;
        }        
        
        if ($task === "import") {
            $checkResult = $checker->check();

            if (count($checkResult) > 0) {
                $helper = $this->getHelper('question');

                $question = new ConfirmationQuestion("\nVarious objects werde changed in local system (see above) - update anyway (yes|no)? \n", false);

                if (!$helper->ask($input, $output, $question)) {
                    return;
                }
            }

            if ($options["no-backup"] === false) {
                $backup = new Backup($config);

                $backup->backup();
            }

            $import = new Import($config);

            $import->import();
        }

        if ($task === "export") {
            $export->export();
        }

        if ($task === "check") {
            $checker->check();
        }        
        
        CliHelper::saveFile(\TrClassUpdateBundle\Library\Helper::getLogPath() . "/actions.log");
    }
}
