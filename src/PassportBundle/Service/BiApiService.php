<?php

namespace PassportBundle\Service;

use Pimcore\Model\DataObject;

class BiApiService
{
    /**
     * @param array $tokenData
     * @return array
     */
    public function getUserData(array $tokenData): array
    {
        $userData = [
            'user_exists' => false,
            'email' => '',
            'first_name' => '',
            'last_name' => '',
            'organisation_id' => '',
        ];

        if (!$tokenData['email']) {
            return $userData;
        }

        $user = DataObject\Customer::getByEmail($tokenData['email'], 1);

        if (!$user) {
            return $userData;
        }

        $organisationIds = [];
        foreach ($user->getOrganisationId() as $organisationId) {
            $organisationIds[] = $organisationId[0];
        }

        $userData['user_exists'] = true;
        $userData['email'] = $user->getEmail() ?: '';
        $userData['first_name'] = $user->getFirstname() ?: '';
        $userData['last_name'] = $user->getLastname() ?: '';
        $userData['organisation_id'] = $organisationIds;

        return $userData;
    }
}
