<?php

namespace PassportBundle\Service;

use Pimcore\Model\WebsiteSetting;
use Pimcore\Model\DataObject\Configuration;
use Exception;

class ConfigService
{
    private $configurations;

    public function getConfiguration() {
        return $this->getConfigurationByKey("configuration_id");
    }

    /**
     * @param string $key
     * @return \Pimcore\Model\DataObject\Concrete|Configuration
     * @throws Exception
     */
    public function getConfigurationByKey(string $key) {
        if($this->configurations[$key]) {
            return $this->configurations[$key];
        }

        if(!WebsiteSetting::getByName($key)) {
            throw new Exception("Key '{$key}' does not exist in configuration object!");
        }

        $configurationId = WebsiteSetting::getByName($key)->getData();

        $configuration = Configuration::getById($configurationId);

        if(!$configuration) {
            throw new Exception("Configuration with id {$configurationId} could not be loaded!");
        }

        $this->configurations[$key] = $configuration;

        return $configuration;
    }
}
