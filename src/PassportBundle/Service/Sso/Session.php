<?php

namespace PassportBundle\Service\Sso;

use Pimcore\Config;
use Pimcore\Db\Connection;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Application;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class Session
{
    /**
     * @var Connection
     */
    private $databaseConnection;

    /**
     * @var ParameterBagInterface
     */
    private $parameters;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var Request
     */
    private $request;

    /** @var UrlGeneratorInterface $urlGenerator */
    private $urlGenerator;

    /** @var SessionInterface $session */
    private $session;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /** @var SsoUserService $ssoUserService */
    private $ssoUserService;

    const SESSION_DATABASE_TABLE = 'sso_master_sessions';

    public function __construct(
        Connection $database,
        ParameterBagInterface $parameters,
        Security $security,
        RequestStack $requestStack,
        UrlGeneratorInterface $urlGenerator,
        SessionInterface $session,
        TokenStorageInterface $tokenStorage,
        SsoUserService $ssoUserService
    ) {
        $this->databaseConnection = $database;

        $this->parameters = $parameters;

        $this->security = $security;

        if($requestStack && $requestStack->getMasterRequest()) {
            $this->request = $requestStack->getMasterRequest();

            $this->locale = $this->request->getLocale();
        }

        $this->urlGenerator = $urlGenerator;
        $this->session = $session;
        $this->ssoUserService = $ssoUserService;

        $this->tokenStorage = $tokenStorage;
    }

    public function handleLoginStatus() {
        $table = self::SESSION_DATABASE_TABLE;

        $sessionId = $this->session->getId();

        $entry = $this->databaseConnection->fetchRow("SELECT * FROM {$table} WHERE internal_session = ? AND $table.system = 'usermanagement'", [$sessionId]);

        if(is_array($entry) && is_object($this->security->getUser())) {
            return true;
        }

        if(!is_array($entry) && is_object($this->security->getUser())) {
            $this->tokenStorage->setToken(null);

            $this->session->clear();

            header("Location: /");
            exit;
        }
    }

    public function deleteDataByInternalSessionId(string $internalSessionId)
    {
        $table = self::SESSION_DATABASE_TABLE;

        $this->databaseConnection->query("DELETE FROM $table WHERE internal_session = :sessionId", ['sessionId' => $internalSessionId]);
    }

    public function deleteDataByExternalSessionIdAndSystem(string $externalSessionId, string $systemId)
    {
        $table = self::SESSION_DATABASE_TABLE;

        $query = "DELETE FROM $table WHERE external_session = :sessionId AND $table.system = :system";

        $this->databaseConnection->query($query, ['sessionId' => $externalSessionId, 'system' => $systemId]);
    }

    public function createFromArray($data) {
        $this->create($data);
    }

    public function createFromRequest(Request $request)
    {
        $data = [
            'external_session' => $request->get("token") ?? 0,
            'internal_session' => $request->getSession()->getId(),
            'system' => $request->get("system-id") ?? 'usermanagement',
            'checksum' => $request->get("checksum") ?? null,
            'user' => $this->getUserData()
        ];

        $this->create($data);
    }

    public function getUserData()
    {
        $user = $this->security->getUser();

        $userAsArray = (array)$user;

        $userData = [];
        foreach ($userAsArray as $key => $entry) {
            $key = str_replace("\x00*\x00", "", $key);

            if (!in_array($key, ['o_class', 'dao', 'password', 'avatar'])) {
                $userData[$key] = $entry;
            }

            if ($key == 'ApplicationsData') {
                $applicationsData = [];

                if (is_array($entry)) {
                    foreach ($entry as $applicationData) {
                        $applicationsData[] = [
                            'application_id' => $applicationData['ApplicationId'] ? $applicationData['ApplicationId']->getData() : '',
                            'role_id' => $applicationData['RoleId'] ? $applicationData['RoleId']->getData() : '',
                            'sales_office_id' => $applicationData['SalesOfficeId'] ? $applicationData['SalesOfficeId']->getData() : '',
                        ];
                    }
                }

                $userData[$key] = $applicationsData;
            }
        }

        $userData['AllowedApplications'] = $this->ssoUserService->buildAllowedApplicationsData($user);

        return $userData;
    }

    public function appIsAllowedForUser($applicationId) {
        if(!$applicationId) {
            return false;
        }

        $user = $this->security->getUser();

        foreach ($user->getAllowedApplications() as $allowedApplication) {
            if($applicationId === $allowedApplication->getSystemId() && is_string($allowedApplication->getSystemId())) {
                return true;
            }
        }

        return false;
    }

    public function getAllowedUserApplications () {
        $user = $this->security->getUser();

        if(!$user->getAllowedApplications()) {
            return [];
        }

        $userApplications = [];
        foreach ($user->getAllowedApplications() as $userApplication) {
            /* @var $userApplication \Pimcore\Model\DataObject\Application */

            $userApplications[] = [
                'image' => $userApplication->getLogo()->getImage()->getRealFullPath(),
                'app_name' => $userApplication->getName($this->locale),
                'description' => $userApplication->getDescriptionShort($this->locale),
                'link' => $this->buildLoginLinkForApplication($userApplication),
            ];
        }

        return $userApplications;
    }

    /**
     * @param Application $application
     * @return string
     */
    public function buildLoginLinkForApplication(Application $application): string
    {
        return $this->urlGenerator->generate('sso_application_login', [
            'systemId' => $application->getSystemId(),
        ]);
    }

    public function getBySessionIds($internalSessionId, $externalSessionId)
    {
        $table = self::SESSION_DATABASE_TABLE;

        $query = "SELECT * FROM $table WHERE internal_session = :internal AND external_session = :external";

        $params = [
            'internal' => $internalSessionId,
            'external' => $externalSessionId
        ];

        return $this->databaseConnection->fetchRow($query, $params);
    }

    public function getByExternalSessionIdAndSystemId($externalSessionId, $systemId)
    {
        $table = self::SESSION_DATABASE_TABLE;

        $query = "SELECT * FROM $table WHERE $table.system = :system AND external_session = :external";

        $params = [
            'system' => $systemId,
            'external' => $externalSessionId
        ];

        $row = $this->databaseConnection->fetchRow($query, $params);

        $user = json_decode($row['user']);

        $organisationIds = [];
        foreach ($user->OrganisationId as $organisationId) {
            $organisationIds[] = $organisationId[0];
        }

        $user->OrganisationId = $organisationIds;
        $row['user'] = json_encode($user);

        return $row;
    }

    public function create($data)
    {
        $data = $this->normalizeSessionDataArray($data);

        return $this->databaseConnection->insertOrUpdate(self::SESSION_DATABASE_TABLE, $data);
    }

    public function updateSessionUpdateTimestamp(string $internalSessionId)
    {
        $params = [
            'internal' => $internalSessionId
        ];

        $table = self::SESSION_DATABASE_TABLE;

        $query = "UPDATE $table SET updated = NOW() WHERE internal_session = :internal";

        $this->databaseConnection->query($query, $params);
    }

    public function checksumIsValid(string $systemId, string $token, string $checksum)
    {
        $createdChecksum = $this->createChecksum($systemId, $token);

        return $createdChecksum === $checksum;
    }

    public function createChecksum(string $systemId, string $token)
    {
        $secret = $this->getSecretBySystemId($systemId);

        return hash('sha512', $systemId . $token . $secret);
    }

    public function logInApplication(DataObject\Application $application, string $internalSessionId)
    {
        $websiteConfig = Config::getWebsiteConfig();

        $stagePass = $websiteConfig->get('stage_pass');
        $stageUser = $websiteConfig->get('stage_user');

        $htpasswd = '';
        if ($stagePass && $stageUser) {
            $htpasswd = $stageUser . ':' . $stagePass . '@';;
        }

        $externalSessionId = hash('sha256', uniqid());
        $systemId = $application->getSystemId();
        $applicationSecret = $application->getSecret();
        $checksum = hash('sha512', $systemId . $externalSessionId . $applicationSecret);

        $this->create([
            'external_session' => $externalSessionId,
            'internal_session' => $internalSessionId,
            'system' => $systemId,
            'checksum' => $checksum,
            'user' => $this->getUserData()
        ]);

        $parameters = http_build_query([
            'sess_id' => $externalSessionId,
            'checksum' => $checksum,
            'app_type' => $application->getApplicationType(),
        ]);

        $redirectUrl = $application->getDomain() . '?' . $parameters;

        if ($htpasswd !== '') {
            $redirectUrl = str_replace("://", "://$htpasswd", $redirectUrl);
        }

        return $redirectUrl;
    }

    private function getSecretBySystemId(string $systemId)
    {
        $applicationListing = new Application\Listing();

        $applicationListing->addConditionParam("SystemId = :systemId", ["systemId" => $systemId]);

        if (!$applicationListing->current()) {
            throw new \Exception("No application found for system with id '{$systemId}'!");
        }

        return $applicationListing->current()->getSecret();
    }

    private function normalizeSessionDataArray($data)
    {
        if (isset($data['user']) && is_array($data['user'])) {
            $data['user'] = json_encode($data['user']);
        }

        if (!isset($data['created'])) {
            $data['created'] = date("Y-m-d H:i:s");
        }

        if (!isset($data['updated'])) {
            $data['updated'] = date("Y-m-d H:i:s");
        }

        return $data;
    }

}
