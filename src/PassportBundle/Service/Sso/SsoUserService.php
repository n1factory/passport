<?php

namespace PassportBundle\Service\Sso;

use Pimcore\Model\DataObject;

class SsoUserService
{
    /**
     * @param DataObject\Customer $user
     * @return array
     */
    public function buildAllowedApplicationsData(DataObject\Customer $user)
    {
        $allowedApplicationsData = [];

        $allowedApplications = $user->getAllowedApplications();
        $languages =\Pimcore\Tool::getValidLanguages();

        foreach ($allowedApplications as $allowedApplication) {
            $logoLink = $this->generateApplicationLogoLink($allowedApplication);

            $allowedApplicationData = [
                'link' => $allowedApplication->getDomain(),
                'system' => $allowedApplication->getSystemId(),
                'logo_link' => $logoLink,
                'app_type' => $allowedApplication->getApplicationType(),
            ];

            foreach ($languages as $locale) {
                $allowedApplicationData['name'][$locale] = $allowedApplication->getName($locale);
            }

            $allowedApplicationsData[] = $allowedApplicationData;
        }

        return $allowedApplicationsData;
    }

    private function generateApplicationLogoLink(DataObject\Application $application)
    {
        $logo = $application->getLogo();

        if (!$logo) {
            return '';
        }

        $hostUrl = \Pimcore\Tool::getHostUrl();

        return $hostUrl . $logo->getImage()->getFullPath();
    }
}
