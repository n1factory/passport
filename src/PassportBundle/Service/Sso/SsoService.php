<?php

namespace PassportBundle\Service\Sso;

use Pimcore\Model\DataObject;

class SsoService
{
    /** @var Session $sessionService */
    private $sessionService;

    /**
     * SsoService constructor.
     * @param Session $sessionService
     */
    public function __construct(
        Session $sessionService
    ) {
        $this->sessionService = $sessionService;
    }

    public function getApplications()
    {
        $applicationsList = new DataObject\Application\Listing();

        $applicationsData = [];

        foreach ($applicationsList as $application) {
            if ($application->getDomain() && $application->getSystemId()) {
                $applicationsData[$application->getSystemId()] = [
                    'domain' => $application->getDomain(),
                    'secret' => $application->getSecret(),
                ];
            }
        }

        return $applicationsData;
    }

    /**
     * @param string|null $siteId
     * @return mixed|null
     */
    public function getApplicationLinkById(?string $applicationId)
    {
        if (!isset($applicationId)) {
            return null;
        }

        $applicationsList = new DataObject\Application\Listing();
        $applicationsList->setCondition('SystemId = ?', $applicationId);

        $application = $applicationsList->current();

        if ($application && $application->getDomain()) {
            return $application->getDomain();
        }

        return null;
    }

    public function handleLoginStatus() {
        $this->sessionService->handleLoginStatus();
    }

    public function deleteByInternalSessionId (string $internalSessionId) {
        $this->sessionService->deleteDataByInternalSessionId($internalSessionId);
    }
}
