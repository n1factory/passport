<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace PassportBundle\Service;

use CustomerManagementFrameworkBundle\Model\CustomerInterface;
use Factory\UserManagementBundle\Service\InvitationStatusOptionsProvider;
use Pimcore\Model\DataObject;
use Symfony\Component\Form\Form;

class RegistrationFormHandler
{
    /** @var ApplicationInvitationService $applicationInvitationService */
    private $applicationInvitationService;

    /**
     * RegistrationFormHandler constructor.
     * @param ApplicationInvitationService $applicationInvitationService
     */
    public function __construct(ApplicationInvitationService $applicationInvitationService)
    {
        $this->applicationInvitationService = $applicationInvitationService;
    }

    protected function getFormDataMapping(): array {
        return [
            'salutation' => [
                'object' => 'no',
                'field' => 'salutation',
            ],
            "register_firstname" => [
                "object" => "no",
                "field" => "firstname",
            ],
            "register_lastname" => [
                "object" => "no",
                "field" => "lastname"
            ],
            "register_email" => [
                "object" => "no",
                "field" => "email"
            ],
            "phone" => [
                "object" => "no",
                "field" => "phone"
            ],
            "phone_country" => [
                "object" => "no",
                "field" => "phoneCountry"
            ],
            "mobile" => [
                "object" => "no",
                "field" => "mobile"
            ],
            "mobile_country" => [
                "object" => "no",
                "field" => "mobileCountry"
            ],
            "fax" => [
                "object" => "no",
                "field" => "fax"
            ],
            "fax_country" => [
                "object" => "no",
                "field" => "faxCountry"
            ],
            "initialToken" => [
                "object" => "yes",
                "field" => "InitialToken"
            ]
        ];
    }

    protected function getCustomerMapping(): array
    {
        $mapping = $this->getFormDataMapping();
        $mapping["customerPassword"] = [
            "object" => "no",
            "field" => "password"
        ];

        return $mapping;
    }

    /**
     * Builds initial form data
     *
     * @param CustomerInterface $customer
     *
     * @return array
     */
    public function buildFormData(CustomerInterface $customer): array
    {
        $formData = [];
        foreach ($this->getFormDataMapping() as $formField => $customerProperty) {
            $getter = 'get' . ucfirst($customerProperty["field"]);

            $value = $customer->$getter();

            if (!$value) {
                continue;
            }

            $formData[$formField] = $value;
        }

        return $formData;
    }

    /**
     * Maps form values to customer
     *
     * @param CustomerInterface $customer
     * @param Form $form
     * @throws \Exception
     */
    public function updateCustomerFromForm(CustomerInterface $customer, Form $form)
    {
        if (!$form->isSubmitted() || !$form->isValid()) {
            throw new \RuntimeException('Form must be submitted and valid to apply form data');
        }

        $formData = $form->getData();

        foreach ($this->getCustomerMapping() as $formField => $customerProperty) {
            $value = $formData[$formField] ?? null;

            $setter = 'set' . ucfirst($customerProperty["field"]);

            if (!$value) {
                continue;
            }

            $customer->$setter($value);
        }

        $customer->setFirstRegistration(new \DateTime());

        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $customer->setRegistrationProxy($_SERVER['HTTP_X_FORWARDED_FOR']);
        }

        $customer->setRegistrationIPAddress($_SERVER['REMOTE_ADDR'] . ':' . $_SERVER['REMOTE_PORT']);

        // Handle application data
        if ($formData['application_token']) {
            $invitation = $this->applicationInvitationService->getByTokenEmailCombination(
                $formData['application_token'],
                $formData['register_email']
            );

            if ($invitation) {
                $userApplicationItems = $invitation->getChildren();

                $customerApplicationsData = $customer->getApplicationsData();
                $customerAllowedApplications = $customer->getAllowedApplications();


                foreach ($userApplicationItems as $userApplicationItem) {
                    $application = $userApplicationItem->getApplication();
                    $applicationId = $application->getSystemId();
                    $roleId = $userApplicationItem->getRoleId();
                    $salesOfficeId = $userApplicationItem->getSalesOfficeId();

                    $blockData = [
                        'ApplicationId' => new DataObject\Data\BlockElement('ApplicationId', 'input', $applicationId),
                        'RoleId' => new DataObject\Data\BlockElement('RoleId', 'input', $roleId),
                        'SalesOfficeId' => new DataObject\Data\BlockElement('SalesOfficeId', 'input', $salesOfficeId),
                    ];

                    $customerApplicationsData[] = $blockData;
                    $customerAllowedApplications[] = $application;
                }

                $customer->setApplicationsData($customerApplicationsData)
                    ->setAllowedApplications($customerAllowedApplications)
                    ->setOrganisationId([[$invitation->getOrganisationId()]])
                ;
            }
        }
    }
}
