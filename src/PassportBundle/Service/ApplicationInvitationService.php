<?php

namespace PassportBundle\Service;

use PassportBundle\Repository\ApplicationRepositoryInterface;
use Pimcore\Log\Simple;
use Pimcore\Mail;
use Pimcore\Model\DataObject;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ApplicationInvitationService
{
    private static $applicationInvitationsFolder = 'ApplicationInvitations';

    /** @var UserService $userService */
    private $userService;

    /** @var UrlGeneratorInterface $urlGenerator */
    private $urlGenerator;

    /** @var ParameterBagInterface $parameterBag */
    private $parameterBag;

    /** @var UserApplicationItemService $userApplicationItemService */
    private $userApplicationItemService;

    /** @var ApplicationRepositoryInterface $applicationRepository */
    private $applicationRepository;

    /**
     * ApplicationInvitationService constructor.
     * @param UserService $userService
     * @param UrlGeneratorInterface $urlGenerator
     * @param ParameterBagInterface $parameterBag
     * @param ApplicationRepositoryInterface $applicationRepository
     * @param UserApplicationItemService $userApplicationItemService
     */
    public function __construct(
        UserService $userService,
        UrlGeneratorInterface $urlGenerator,
        ParameterBagInterface $parameterBag,
        ApplicationRepositoryInterface $applicationRepository,
        UserApplicationItemService $userApplicationItemService
    ) {
        $this->userService = $userService;
        $this->urlGenerator = $urlGenerator;
        $this->parameterBag = $parameterBag;
        $this->applicationRepository = $applicationRepository;
        $this->userApplicationItemService = $userApplicationItemService;
    }

    /**
     * @param array $requestParams
     * @throws \Exception
     */
    public function handleInvitationForApplication(array $requestParams)
    {
        $user = $this->userService->getUserByEmail($requestParams['email']);
        $application = $this->applicationRepository->getBySystemId($requestParams['application_id']);

        if (!$application) {
            return;
        }

        // If user does not exist in system invite him, else just append application to user
        if (!$user instanceof DataObject\Customer) {
            $invitation = $this->inviteUser($requestParams);
        } else {
            $this->userService->inviteExistingUser($user, $application, $requestParams);
        }

        $this->sendInvitationEmail($user, $application, $invitation);
    }

    /**
     * @param array $params
     * @return DataObject\Invitation
     * @throws \Exception
     */
    public function inviteUser(array $params)
    {
        $invitation = $this->getInvitationByEmail($params['email']);

        // If invitation doesn't already exists create it
        if (!$invitation) {
            $invitation = $this->createInvitation();
            $token = $this->makeInvitationToken($params['application_id']);

            $invitation->setValues([
                'firstName' => $params['first_name'],
                'lastName' => $params['last_name'],
                'email' => $params['email'],
                'accountOwner' => $params['first_name'],
                'invitationTokenKey' => $token,
                'organisationId' => $params['organisation_id']
            ])->save();
        }

        $this->userApplicationItemService->createApplicationItem([
            'application_id' => $params['application_id'],
            'role_id' => $params['role'],
            'sales_office_id' => $params['sales_office'],
            'parent' => $invitation,
        ]);

        return $invitation;
    }

    /**
     * @return DataObject\Invitation
     * @throws \Exception
     */
    public function createInvitation()
    {
        $parent = DataObject\Service::createFolderByPath(self::$applicationInvitationsFolder);

        return DataObject\Invitation::create([
            'key' => 'invitation-' . time(),
            'parent' => $parent,
            'published' => true,
        ]);
    }

    /**
     * @param string $applicationSystemId
     * @return string
     */
    private function makeInvitationToken(string $applicationSystemId)
    {
        $db = \Pimcore\Db::get();
        $invitationsTable = 'object_query_' . DataObject\Invitation::classId();

        $query = "SELECT InvitationTokenKey FROM $invitationsTable";
        $tokensInDb = $db->fetchArray($query) ?: [];

        $applicationName = 'myN1';
        $tokenNumber = rand(100000, 999999);
        $token = $applicationName . $tokenNumber;

        while (in_array($token, $tokensInDb)) {
            $tokenNumber = rand(100000, 999999);
            $token = $applicationName . $tokenNumber;
        }

        return $token;
    }

    private function sendInvitationEmail(
        ?DataObject\Customer $user,
        DataObject\Application $application,
        ?DataObject\Invitation $invitation
    ) {
        $locale = \Pimcore\Tool::getDefaultLanguage();

        $mail = new Mail();

        if ($user) {
            $emailDocument = $application->getInviteExistingUserEmail($locale);

            $mailParams = [
                'bvg_link' => $application->getDomain(),
            ];

            $toEmail = $user->getEmail();
        } else {
            $token = $invitation->getInvitationTokenKey();
            $emailDocument = $application->getInviteNewUserEmail($locale);

            $toEmail = $invitation->getEmail();

            $registerLink = $this->generateRegistrationLink($token);

            $mailParams = [
                'invitation_token' => $token,
                'register_link' => $registerLink,
            ];
        }

        try {
            $mail
                ->setTo($toEmail)
                ->setParams($mailParams)
                ->setDocument($emailDocument);

            $mail->send();
        } catch (\Exception $e) {
            Simple::log('user_invite', 'Email to user ' . $toEmail . ' not sent: ' . $e->getMessage());
            throw new \Exception('Sending email failed.');
        }
    }

    /**
     * @param string $email
     * @return DataObject\Invitation|DataObject\Invitation\Listing
     */
    private function getInvitationByEmail(string $email)
    {
        return DataObject\Invitation::getByEmail($email, 1);
    }

    /**
     * @param string $hashedToken
     * @return DataObject\Invitation
     */
    public function getByHashedToken(string $hashedToken)
    {
        $secret = $this->parameterBag->get('secret');
        $invitationsList = new DataObject\Invitation\Listing();
        $invitationsTable = 'object_query_' . DataObject\Invitation::classId();

        $invitationsList->setCondition("
            o_id IN (
                SELECT oo_id FROM $invitationsTable WHERE SHA2(CONCAT(InvitationTokenKey, '$secret'), 512) = ?
            )
        ", [$hashedToken]);

        return $invitationsList->current();
    }

    /**
     * @param string $token
     * @param string $email
     * @return DataObject\Invitation
     */
    public function getByTokenEmailCombination(string $token, string $email)
    {
        $invitationsList = new DataObject\Invitation\Listing();

        $invitationsList->setCondition(
            'email = ? AND InvitationTokenKey = ?',
            [$email, $token]
        );

        return $invitationsList->current();
    }

    /**
     * @param string $token
     * @return string
     */
    private function generateRegistrationLink(string $token)
    {
        $secret = $this->parameterBag->get('secret');

        $hashedToken = hash('sha512', $token . $secret);

        return $this->urlGenerator->generate(
            'register',
            [
                '_application_token' => $hashedToken
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }
}

