<?php

namespace PassportBundle\Service;

use PassportBundle\Repository\ApplicationRepositoryInterface;
use PassportBundle\Service\Sso\SsoService;
use PassportBundle\Service\Sso\SsoUserService;
use Pimcore\Config;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpClient\HttpClient;

class UserService
{
    /** @var string $updateUrl */
    private static $updateUrl = '/api/user/update';

    /** @var SsoService $ssoService */
    private $ssoService;

    /** @var ApplicationRepositoryInterface $applicationRepository */
    private $applicationRepository;

    /** @var SsoUserService $ssoUserService */
    private $ssoUserService;

    /**
     * UserService constructor.
     * @param SsoService $ssoService
     * @param ApplicationRepositoryInterface $applicationRepository
     */
    public function __construct(
        SsoService $ssoService,
        ApplicationRepositoryInterface $applicationRepository,
        SsoUserService $ssoUserService
    ) {
        $this->ssoService = $ssoService;
        $this->applicationRepository = $applicationRepository;
        $this->ssoUserService = $ssoUserService;
    }

    /**
     * @param int $userId
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function updateUserById(int $userId)
    {
        $user = $this->getUserById($userId);

        if (empty($user)) {
            return;
        }

        $websiteConfig = Config::getWebsiteConfig();

        $stagePass = $websiteConfig->get('stage_pass');
        $stageUser = $websiteConfig->get('stage_user');

        if (!$stagePass || !$stageUser) {
            $authBasic = [];
        } else {
            $authBasic = [
                $stageUser,
                $stagePass
            ];
        }

        $httpClient = HttpClient::create();

        $ssoSites = $this->ssoService->getApplications();

        $token = uniqid('', true);

        $allowedApplicationsData = $this->ssoUserService->buildAllowedApplicationsData($user);

        foreach ($ssoSites as $siteName => $siteData) {
            if (empty($siteData['domain'])) {
                continue;
            }

            $checksum = hash('sha512', $token . $siteData['secret']);

            $requestData['body'] = [
                'user_data' => [
                    'salutation' => $user->getSalutation(),
                    'title' => $user->getTitle() ?: '',
                    'email' => $user->getEmail(),
                    'first_name' => $user->getFirstname(),
                    'last_name' => $user->getLastname(),
                    'phone' => $user->getPhone() ?: '',
                    'phone_country' => $user->getPhoneCountry(),
                    'mobile' => $user->getMobile(),
                    'mobile_country' => $user->getMobileCountry(),
                    'fax' => $user->getFax() ?: '',
                    'fax_country' => $user->getFaxCountry(),
                    'allowed_applications' => $allowedApplicationsData,
                ],
                'token' => $token,
                'checksum' => $checksum,
            ];

            if ($authBasic) {
                $requestData['auth_basic'] = $authBasic;
            }

            try {
                $httpClient->request(
                    'POST',
                    $siteData['domain'] . self::$updateUrl,
                    $requestData
                );
            } catch (\Exception $e) {
                Simple::log('user_update', $e->getMessage());
            }
        }
    }

    /**
     * @param string $email
     * @param string $applicationId
     * @throws \Exception
     */
    public function removeApplicationFromUser(string $email, string $applicationId)
    {
        $user = $this->getUserByEmail($email);

        $allowedApplications = $user->getAllowedApplications();

        foreach ($allowedApplications as $key => $allowedApplication) {
            if ($allowedApplication->getSystemId() == $applicationId) {
                unset($allowedApplications[$key]);
            }
        }

        $user->setAllowedApplications($allowedApplications)
            ->save();
    }

    /**
     * @param string $email
     * @param string $applicationId
     * @throws \Exception
     */
    public function assignApplicationToUser(string $email, string $applicationId)
    {
        $user = $this->getUserByEmail($email);
        $application = $this->applicationRepository->getBySystemId($applicationId);

        $allowedApplications = $user->getAllowedApplications();

        $allowedApplications[] = $application;

        $user->setAllowedApplications($allowedApplications)
            ->save();
    }

    /**
     * @param DataObject\Customer $user
     * @param DataObject\Application|null $application
     * @param array $requestParams
     * @throws \Exception
     */
    public function inviteExistingUser(
        DataObject\Customer $user,
        DataObject\Application $application,
        array $requestParams
    ) {
        $usersApplications = $user->getAllowedApplications();

        // Check if application already set to user
        foreach ($usersApplications as $usersApplication) {
            if ($usersApplication->getId() === $application->getId()) {
                return;
            }
        }

        $usersApplications[] = $application;

        // Handle applications data
        $blockData = [
            'ApplicationId' => new DataObject\Data\BlockElement(
                'ApplicationId',
                'input',
                $requestParams['application_id']
            ),
            'RoleId' => new DataObject\Data\BlockElement(
                'RoleId',
                'input',
                $requestParams['role']
            ),
            'SalesOfficeId' => new DataObject\Data\BlockElement(
                'SalesOfficeId',
                'input',
                $requestParams['sales_office']
            ),
        ];

        $customerApplicationsData = $user->getApplicationsData();
        $customerApplicationsData[] = $blockData;
        $organisationIds = array_merge($user->getOrganisationId() ?? [], [[$requestParams['organisation_id']]]);

        $user->setAllowedApplications($usersApplications)
            ->setApplicationsData($customerApplicationsData)
            ->setOrganisationId($organisationIds)
            ->save();
    }

    /**
     * @param int $id
     * @return DataObject\AbstractObject|DataObject\Concrete|DataObject\Customer|null
     */
    public function getUserById(int $id)
    {
        return DataObject\Customer::getById($id);
    }

    /**
     * @param string $email
     * @return DataObject\Customer|DataObject\Customer\Listing
     */
    public function getUserByEmail(string $email)
    {
        return DataObject\Customer::getByEmail($email, 1);
    }
}
