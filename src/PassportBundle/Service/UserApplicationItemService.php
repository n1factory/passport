<?php

namespace PassportBundle\Service;

use PassportBundle\Repository\ApplicationRepositoryInterface;
use Pimcore\Model\DataObject;

class UserApplicationItemService
{
    /** @var ApplicationRepositoryInterface $applicationRepository */
    private $applicationRepository;

    /**
     * UserApplicationItemService constructor.
     * @param ApplicationRepositoryInterface $applicationRepository
     */
    public function __construct(ApplicationRepositoryInterface $applicationRepository)
    {
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * @param array $params
     * @return DataObject\UserApplicationItem
     * @throws \Exception
     */
    public function createApplicationItem(array $params): DataObject\UserApplicationItem
    {
        $applicationSystemId = $params['application_id'];
        $application = $this->applicationRepository->getBySystemId($applicationSystemId);

        return DataObject\UserApplicationItem::create([
            'key' => $params['application_id'] . '_' . uniqid(),
            'parent' => $params['parent'],
            'published' => true,
            'application' => $application,
            'RoleId' => $params['role_id'],
            'SalesOfficeId' => $params['sales_office_id'],
        ])->save();
    }
}
