<?php

namespace PassportBundle\Service;

use Pimcore\Model\DataObject\Customer;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class PasswordService
{
    private $encoder;

    public function __construct(
        EncoderFactoryInterface $encoder
    ) {
        $this->encoder = $encoder;
    }

    public function generateRandomPassword(Customer $customer, int $length = 8)
    {
        $chars = "abcdefghijklmnopqrstuvwxyz";
        $capChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $numbers = "0123456789";

        $charNr = rand(1, $length - 2);
        $capsCharNr = rand(1, $length - 1 - $charNr);
        $numbersNr = $length - $charNr - $capsCharNr;

        $randomChars = substr(str_shuffle($chars), 0, $charNr);
        $randomCapsChars = substr(str_shuffle($capChars), 0, $capsCharNr);
        $randomNumbers = substr(str_shuffle($numbers), 0, $numbersNr);

        $temporaryPassword = substr(str_shuffle($randomChars . $randomCapsChars . $randomNumbers), 0, $length);

        $customerEncoder = $this->encoder->getEncoder($customer);

        $hashedPassword = $customerEncoder->encodePassword($temporaryPassword, $customer->getUsername());

        $customer->setPassword($hashedPassword)->save();

        return $temporaryPassword;
    }
}
