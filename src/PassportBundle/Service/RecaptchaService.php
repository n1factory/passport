<?php

namespace PassportBundle\Service;

use GuzzleHttp\Client;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\RequestStack;

class RecaptchaService
{
    private $request;

    private $guzzleClient;

    public function __construct(
        RequestStack $requestStack
    )
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->guzzleClient = new Client();
    }

    public function validateResponse()
    {
        $recaptchaSecretKeySetting = WebsiteSetting::getByName('recaptcha_secret_key');

        if ($recaptchaSecretKeySetting instanceof WebsiteSetting) {
            $recaptchaSecretKey = WebsiteSetting::getByName('recaptcha_secret_key')->getData();

            if (!empty($recaptchaSecretKey)) {
                if (!empty($this->request->get('g-recaptcha-response'))) {
                    $captcha = $this->request->get('g-recaptcha-response');

                    $response = $this->guzzleClient->request(
                        'POST',
                        "https://www.google.com/recaptcha/api/siteverify",
                        [
                            'secret' => $recaptchaSecretKey,
                            'response' => $captcha
                        ]
                    );

                    $data = json_decode($response->getBody(), true);

                    return $data['success'];
                }
            }
        }

        return true;
    }
}
