<?php

namespace PassportBundle\Service\OptionsProvider;

use Pimcore\Model\DataObject\Application;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\MultiSelectOptionsProviderInterface;
use Pimcore\Model\DataObject\Role;

class RolePermissionsOptionsProvider implements MultiSelectOptionsProviderInterface
{
    /**
     * Define options in child class
     * @var array
     */
    public static $options = [];

    public function getOptions($context, $fieldDefinition)
    {
        $options = [];

        /** @var Role $object */
        $object = $context['object'];

        if ($object) {
            $application = $object->getParent();

            if ($application instanceof Application) {
                $permissions = array_column($application->getApplicationPermissions() ?: [], 'permission');

                sort($permissions);

                foreach ($permissions as $permission) {
                    $options[] = [
                        'key' => $permission,
                        'value' => $permission,
                    ];
                }
            }
        }

        return $options;
    }

    public function hasStaticOptions($context, $fieldDefinition)
    {
        return true;
    }
}
