<?php

namespace PassportBundle\Service;

use Carbon\Carbon;
use PassportBundle\Service\ConfigService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

class BiApiTokenService
{
    /** @var string */
    private $secret;

    /** @var int */
    private $prefixLength;

    /** @var int */
    private $validTime;

    public function __construct(ParameterBagInterface $parameterBag, ConfigService $configService)
    {
        $this->secret = $parameterBag->get('bi_api_secret');

        $configuration = $configService->getConfiguration();

        $this->prefixLength = $configuration->getBiApiPrefixLength() ?: 9;
        $this->validTime = $configuration->getTokenValidTime() ?: 3600;
    }

    public function authorizeRequest(Request $request)
    {
        $bearerToken = $request->headers->get('Authorization');

        if (!$bearerToken) {
            return false;
        }

        $token = str_replace('Bearer ', '', $bearerToken);

        return $this->isValid($token) ? $token : false;
    }

    /**
     * @param array $payload
     * @return string
     */
    public function generate(array $payload): string
    {
        $prefix = $this->generatePrefix();

        $payload['iat'] = Carbon::now()->timestamp;

        $encodedPayload = $this->encodePayload($payload);
        $signature = $this->generateSignature($encodedPayload);

        return "$prefix$encodedPayload.$signature";
    }

    /**
     * @param string $token
     * @return bool
     */
    public function isValid(string $token): bool
    {
        $token = $this->removePrefix($token);

        $encodedPayload = $this->getEncodedPayload($token);
        $signature = $this->getSignature($token);

        $signatureCheck = $this->generateSignature($encodedPayload);

        if ($signatureCheck !== $signature) {
            return false;
        }

        $payload = $this->decodePayload($encodedPayload);

        if (
            !$payload ||
            Carbon::createFromTimestamp($payload['iat'])->addSeconds($this->validTime)->lt(Carbon::now())
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param string $token
     * @return array
     */
    public function getPayload(string $token): array
    {
        $token = $this->removePrefix($token);

        return $this->decodePayload($this->getEncodedPayload($token));
    }

    /**
     * @param string $token
     * @return string
     */
    private function getSignature(string $token): string
    {
        $explodedToken = explode('.', $token);

        return $explodedToken[1];
    }

    /**
     * @param string $token
     * @return string
     */
    private function getEncodedPayload(string $token): string
    {
         $explodedToken = explode('.', $token);

         return $explodedToken[0];
    }

    /**
     * @param string $encodedPayload
     * @return string
     */
    private function generateSignature(string $encodedPayload): string
    {
        return hash('sha256', $encodedPayload . $this->secret);
    }

    /**
     * @param array $payload
     * @return string
     */
    private function encodePayload(array $payload): string
    {
        return trim(base64_encode(json_encode($payload)), '=');
    }

    /**
     * @param string $encodedPayload
     * @return array
     */
    private function decodePayload(string $encodedPayload): array
    {
        return json_decode(base64_decode($encodedPayload), true) ?: [];
    }

    /**
     * @param string $token
     * @return string
     */
    private function removePrefix(string $token): string
    {
        return substr($token, $this->prefixLength, strlen($token)) ?: '';
    }

    /**
     * @return string
     */
    private function generatePrefix(): string
    {
        $numbers = '0123456789';
        $chars = 'abcdefghijklmnopqrstuvwxyz';

        $nrOfNumbers =  rand(1, $this->prefixLength - 2);
        $nrOfLowercase = rand(1, $this->prefixLength - 1 -$nrOfNumbers);
        $nrOfUppercase = $this->prefixLength - $nrOfNumbers - $nrOfLowercase;

        $randNumbers = substr(str_shuffle($numbers), 0, $nrOfNumbers);
        $randLowercase = substr(str_shuffle($chars), 0, $nrOfLowercase);
        $randUppercase = substr(str_shuffle(strtoupper($chars)), 0, $nrOfUppercase);

        return str_shuffle("$randLowercase$randNumbers$randUppercase");
    }
}
