<?php

namespace PassportBundle\Service;

use PassportBundle\Helper\Server;
use Pimcore\Model\DataObject;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmailService
{
    /** @var DataObject\Configuration  */
    private $config;

    /** @var TranslatorInterface  */
    private $translator;

    /** @var ContainerInterface  */
    private $container;


    public function __construct(
        TranslatorInterface $translator,
        ConfigService $configService
    ) {
        $this->config = $configService->getConfiguration();
        $this->translator = $translator;
    }

    /**
     * @param string locale
     * @param array $userData
     * @throws \Exception
     */
    public function sendUserRegistrationConfirmationMail($locale = 'de_DE', $userData = [])
    {
        $customerEmail = $userData['email'];

        $firstName = $userData['firstname'];
        $lastName = $userData['lastname'];

        $mail = new \Pimcore\Mail($this->translator->trans('user.registration.confirmation.mail.subject', [], null, $locale));

        $mail->addTo($customerEmail);

        $documentObject = $this->config->getUserRegistrationConfirmationEmail($locale);

        $mail->setDocument($documentObject);

        $mail->setParams([
            'login_link' => $this->getDomain() . "?email=" .urlencode($customerEmail),
            'firstname' => $firstName,
            'lastname' => $lastName,
            'email' => $customerEmail
        ]);

        $mail->send();
    }

    /**
     * @param DataObject\Customer $customer
     * @param string $temporaryPassword
     * @param string $locale
     * @throws \Exception
     */
    public function sendPasswordRecoveryEmail(DataObject\Customer $customer, string $temporaryPassword, string $locale = 'de_DE')
    {
        $mail = new \Pimcore\Mail($this->translator->trans('customer.password.recovery.mail.subject', [], null, $locale));

        $documentObject = $this->config->getPasswordRecoveryEmail($locale);

        $mail->addTo($customer->getEmail());

        $mail->setDocument($documentObject);

        $mail->setParams([
            'name' => $customer->getEmail(),
            'temp_password' => $temporaryPassword,
            'login_link' => '/',
        ]);

        $mail->send();
    }

    /**
     * @return string
     */
    private function getDomain()
    {
        return Server::getBaseUrl();
    }
}
