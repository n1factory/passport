<?php

declare(strict_types=1);

/**
 * Pimcore Customer Management Framework Bundle
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (C) Elements.at New Media Solutions GmbH
 * @license    GPLv3
 */

namespace PassportBundle\Form;

use Pimcore\Model\DataObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class UserRegistration extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $translator = $options['data']['translator'];
        $userData = $options['data']['user_data'];
        $formSelectsOptions = $this->getFormOptions();

        $builder
            ->add('salutation', Type\ChoiceType::class, [
                'label' => 'user_form.label.salutation',
                'choices' => $formSelectsOptions['salutation_options'],
                'placeholder' => false,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => $translator->trans('register.firstname.required.salutation')
                    ])
                ]
            ])
            ->add('register_firstname', TextType::class, [
                'attr' => [
                    'placeholder' => 'register.firstname_label.text',
                ],
                'data' => $userData['firstname'],
                'constraints' => [
                    new NotBlank([
                        'message' => $translator->trans('register.firstname.required.message')
                    ])
                ]
            ])
            ->add('register_lastname', TextType::class, [
                'attr' => [
                    'placeholder' => 'register.lastname_label.text',
                ],
                'data' => $userData['lastname'],
                'constraints' => [
                    new NotBlank([
                        'message' => $translator->trans('register.lastname.required.message')
                    ])
                ]
            ])
            ->add('phone_country', Type\ChoiceType::class, [
                'label' => 'user_form.label.phone_country',
                'choices' => $formSelectsOptions['phone_country_options'],
                'placeholder' => false,
                'required' => false,
            ])
            ->add('phone', Type\TextType::class, [
                'label' => 'user_form.label.phone',
                'required' => false,
            ])
            ->add('mobile_country', Type\ChoiceType::class, [
                'label' => 'user_form.label.mobile_country',
                'choices' => $formSelectsOptions['fax_country_options'],
                'placeholder' => false,
                'required' => false,
            ])
            ->add('mobile', Type\TextType::class, [
                'label' => 'user_form.label.mobile',
                'required' => false,
            ])
            ->add('fax_country', Type\ChoiceType::class, [
                'label' => 'user_form.label.fax_country',
                'choices' => $formSelectsOptions['fax_country_options'],
                'placeholder' => false,
                'required' => false,
            ])
            ->add('fax', Type\TextType::class, [
                'label' => 'user_form.label.fax',
                'required' => false,
            ])
            ->add('register_email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'register.email_label.text',
                    'class' => 'form-control',
                    'data-validate' => 'required|email',
                ],
                'data' => $userData['email'],
                'constraints' => [
                    new NotBlank([
                        'message' => $translator->trans('.register.email.required.message')
                    ])
                ]
            ])
            ->add('register_password', PasswordType::class, [
                'attr' => [
                    'placeholder' => 'register.password_label.text',
                    'pattern' => "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*?[#?!@$%^&*-+'.§{}[\]=`°^-_:;,])((.){8,32})"
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => $translator->trans('register.password.required.message')
                    ]),
                    new Constraints\Regex([
                        'pattern' => '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#\?\!@$%\^&*-\+\'\.§\{\}\[\]=\`°\^-_:;,]).{8,}$/',
                        'message' => $translator->trans("password.pattern.does.not.match"),
                    ]),
                ]
            ])
            ->add('register_password_repeat', PasswordType::class, [
                'attr' => [
                    'placeholder' => 'register.password_repeat_label.text',
                    'pattern' => "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*?[#?!@$%^&*-+'.§{}[\]=`°^-_:;,])((.){8,32})"
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => $translator->trans('register.passsword.repeat.required.message')
                    ]),
                    new Constraints\Regex([
                        'pattern' => '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#\?\!@$%\^&*-\+\'\.§\{\}\[\]=\`°\^-_:;,]).{8,}$/',
                        'message' => $translator->trans("password.pattern.does.not.match"),
                    ]),
                ]
            ])
            /*
            ->add('terms_of_use_n1', CheckboxType::class, [
                'label' => html_entity_decode($translator->trans('register.terms_of_use_n1')),
                'required' => true
            ])
            */
            ->add('privacy_policy_n1', CheckboxType::class, [
                'label' => $translator->trans('register.privacy_policy_n1'),
                'required' => true
            ])
            ->add('initialToken', HiddenType::class, [
                'data' => isset($options['data']['initialTokenValue']) ? $options['data']['initialTokenValue'] : '',
            ])
            ->add('register_submit', SubmitType::class, [
                'label' => 'register.button.text'
            ])
            ->add('application_token', TextType::class, [
                'constraints' => [
                    new Callback([
                        'callback' => [$this, 'validateApplicationToken'],
                    ]),
                ],
                'required' => false
            ]);
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        // we need to set this to an empty string as we want _username as input name
        // instead of register_form[_username] to work with the form authenticator out
        // of the box
        return '';
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * @param $object
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateApplicationToken($object, ExecutionContextInterface $context, $payload)
    {
        $formData = $context->getObject()->getParent()->getNormData();

        if(isset($object)) {
            $invitationsList = new DataObject\Invitation\Listing();

            $invitationsList->setCondition(
                'email = ? AND InvitationTokenKey = ?',
                [$formData['register_email'], $object]
            );

            $invitation = $invitationsList->current();

            if (!$invitation) {
                $context->buildViolation('register_form.invalid_token_email_combination')
                    ->atPath('application_token')
                    ->addViolation();
            }
        }
    }

    /**
     * Fetch form options for selects via class definition
     *
     * @return array
     */
    private function getFormOptions()
    {
        $classDefinition = DataObject\ClassDefinition::getByName('Customer');

        $salutationFieldDefinition = $classDefinition->getFieldDefinition('Salutation');
        $phoneCountryFieldDefinition = $classDefinition->getFieldDefinition('phoneCountry');
        $faxCountryFieldDefinition = $classDefinition->getFieldDefinition('faxCountry');

        $salutationOptions = [];
        $salutationOptions[''] = '';
        foreach ($salutationFieldDefinition->getOptions() as $option) {
            $salutationOptions[$option['key']] = $option['value'];
        }

        $phoneCountryOptions = [];
        foreach ($phoneCountryFieldDefinition->getOptions() as $option) {
            $phoneCountryOptions[$option['key']] = $option['value'];
        }

        $faxCountryOptions = [];
        foreach ($faxCountryFieldDefinition->getOptions() as $option) {
            $faxCountryOptions[$option['key']] = $option['value'];
        }

        return [
            'salutation_options' => $salutationOptions,
            'phone_country_options' => $phoneCountryOptions,
            'fax_country_options' => $faxCountryOptions,
        ];
    }
}
