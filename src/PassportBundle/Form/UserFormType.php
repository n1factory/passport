<?php

namespace PassportBundle\Form;

use Pimcore\Model\DataObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserFormType extends AbstractType
{
    /** @var TranslatorInterface $translator */
    private $translator;

    /**
     * UserFormType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $formSelectsOptions = $this->getFormOptions();

        $builder
            ->add('salutation', Type\ChoiceType::class, [
                'label' => 'user_form.label.salutation',
                'choices' => $formSelectsOptions['salutation_options'],
                'placeholder' => false,
                'required' => false,
            ])
            ->add('title', Type\TextType::class, [
                'label' => 'user_form.label.title',
              /*  'constraints' => [
                    new Constraints\NotBlank([
                        'message' => $this->translator->trans('form.validation.required'),
                    ]),
                ],*/
                'required' => false,
            ])
            ->add('firstname', Type\TextType::class, [
                'label' => 'user_form.label.firstname',
                'constraints' => [
                    new Constraints\NotBlank([
                        'message' => $this->translator->trans('form.validation.required'),
                    ]),
                ],
                'required' => false,
            ])
            ->add('lastname', Type\TextType::class, [
                'label' => 'user_form.label.lastname',
                'constraints' => [
                    new Constraints\NotBlank([
                        'message' => $this->translator->trans('form.validation.required'),
                    ]),
                ],
                'required' => false,
            ])
            ->add('email', Type\EmailType::class, [
                'label' => 'user_form.label.email',
                'constraints' => [
                    new Constraints\NotBlank([
                        'message' => $this->translator->trans('form.validation.required'),
                    ]),
                ],
                'disabled' => true,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('phone_country', Type\ChoiceType::class, [
                'label' => 'user_form.label.phone_country',
                'choices' => $formSelectsOptions['phone_country_options'],
                'constraints' => [
                    new Constraints\NotBlank([
                        'message' => $this->translator->trans('form.validation.required'),
                    ]),
                ],
                'placeholder' => false,
                'required' => false,
            ])
            ->add('phone', Type\TextType::class, [
                'label' => 'user_form.label.phone',
                'required' => false,
            ])
            ->add('mobile_country', Type\ChoiceType::class, [
                'label' => 'user_form.label.mobile_country',
                'choices' => $formSelectsOptions['phone_country_options'],
                'constraints' => [
                    new Constraints\NotBlank([
                        'message' => $this->translator->trans('form.validation.required'),
                    ]),
                ],
                'placeholder' => false,
                'required' => false,
            ])
            ->add('mobile', Type\TextType::class, [
                'label' => 'user_form.label.mobile',
                'required' => false,
            ])
            ->add('fax_country', Type\ChoiceType::class, [
                'label' => 'user_form.label.fax_country',
                'choices' => $formSelectsOptions['fax_country_options'],
                'constraints' => [
                    new Constraints\NotBlank([
                        'message' => $this->translator->trans('form.validation.required'),
                    ]),
                ],
                'placeholder' => false,
                'required' => false,
            ])
            ->add('fax', Type\TextType::class, [
                'label' => 'user_form.label.fax',
                'required' => false,
            ])
            ->add('sso_site_link', Type\HiddenType::class);

    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * Fetch form options for selects via class definition
     *
     * @return array
     */
    private function getFormOptions()
    {
        $classDefinition = DataObject\ClassDefinition::getByName('Customer');

        $salutationFieldDefinition = $classDefinition->getFieldDefinition('Salutation');
        $phoneCountryFieldDefinition = $classDefinition->getFieldDefinition('phoneCountry');
        $faxCountryFieldDefinition = $classDefinition->getFieldDefinition('faxCountry');

        $salutationOptions = [];
        foreach ($salutationFieldDefinition->getOptions() as $option) {
            $salutationOptions[$option['key']] = $option['value'];
        }

        $phoneCountryOptions = [];
        foreach ($phoneCountryFieldDefinition->getOptions() as $option) {
            $phoneCountryOptions[$option['key']] = $option['value'];
        }

        $faxCountryOptions = [];
        foreach ($faxCountryFieldDefinition->getOptions() as $option) {
            $faxCountryOptions[$option['key']] = $option['value'];
        }

        return [
            'salutation_options' => $salutationOptions,
            'phone_country_options' => $phoneCountryOptions,
            'fax_country_options' => $faxCountryOptions,
        ];
    }
}
