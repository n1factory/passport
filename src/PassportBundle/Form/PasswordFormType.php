<?php

namespace PassportBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Contracts\Translation\TranslatorInterface;

class PasswordFormType extends AbstractType
{
    /** @var TranslatorInterface $translator */
    private $translator;

    /**
     * UserFormType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('current_password', Type\PasswordType::class, [
                'label' => 'password_form.label.current_password',
                'constraints' => [
                    new Constraints\NotBlank([
                        'message' => $this->translator->trans('form.validation.required'),
                    ]),
                ],
                'required' => false,
            ])
            ->add('password', Type\RepeatedType::class, [
                'type' => Type\PasswordType::class,
                'constraints' => [
                    new Constraints\NotBlank([
                        'message' => $this->translator->trans('form.validation.required'),
                    ]),
                    new Constraints\Regex([
                        'pattern' => '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#\?\!@$%\^&*-\+\'\.§\{\}\[\]=\`°\^-_:;,]).{8,}$/',
                        'message' => $this->translator->trans('form.validation.invalid-pattern'),
                    ]),
                ],
                'required' => false,
                'first_options' => [
                    'label' => 'password_form.label.new_password',
                ],
                'second_options' => [
                    'label' => 'password_form.label.confirm_password',
                ],
                'invalid_message' => $this->translator->trans('form.validation.inputs-dont-match'),
            ]);
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
