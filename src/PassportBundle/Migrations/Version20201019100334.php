<?php

namespace PassportBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20201019100334 extends AbstractPimcoreMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE `sso_master_sessions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `external_session` VARCHAR(1024) NULL,
  `internal_session` VARCHAR(2048) NULL,
  `system` VARCHAR(3096) NULL,
  `checksum` VARCHAR(4096) NULL,
  `user` TEXT NULL,
  `created` DATETIME NULL,
  `updated` DATETIME NULL,
  PRIMARY KEY (`id`));");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE IF EXISTS `sso_master_sessions`;");

    }
}
