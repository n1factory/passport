<?php

namespace PassportBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210112151557 extends AbstractPimcoreMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
DROP TABLE `sso_master_sessions`;
CREATE TABLE `sso_master_sessions` (
  `external_session` varchar(256) COLLATE utf8mb4_bin NOT NULL,
  `internal_session` varchar(256) COLLATE utf8mb4_bin NOT NULL,
  `system` varchar(256) COLLATE utf8mb4_bin NOT NULL,
  `checksum` varchar(4096) COLLATE utf8mb4_bin DEFAULT NULL,
  `user` text COLLATE utf8mb4_bin,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`system`,`external_session`,`internal_session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
