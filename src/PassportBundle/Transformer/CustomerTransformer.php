<?php

namespace PassportBundle\Transformer;

use Pimcore\Model\DataObject\Customer;

class CustomerTransformer implements CustomerTransformerInterface
{
    /**
     * Maps customer data
     *
     * @param Customer |array|iterable $data
     * @return array
     */
    public function map($data): array
    {
        $factoryData = [];

        if ($data instanceof Customer ) {
            $factoryData = $this->mapCustomer($data);
        }

        if (is_iterable($data)) {
            $factoryData = $this->mapCustomers($data);
        }

        return $factoryData;
    }

    /**
     * Map customer
     *
     * @param Customer $customer
     * @return array
     */
    private function mapCustomer(Customer $customer): array
    {
        return [
            'id' => $customer->getId(),
            'name' => $customer->getFirstname() . ' ' . $customer->getLastname(),
            'email' => $customer->getEmail()
        ];
    }

    /**
     * @param $customers
     * @return array
     */
    private function mapCustomers($customers): array
    {
        $data = [];

        foreach ($customers as $customer) {
            if ($customer instanceof Customer ) {
                $data[] = $this->mapCustomer($customer);
            }
        }

        return $data;
    }

    /**
     * @param Customer |null $user
     * @param string $locale
     * @return array
     */
    public function transformForForm(?Customer $user, string $locale): array
    {
        if (!$user) {
            return [];
        }

        return [
            'id' => $user->getId(),
            'salutation' => $user->getSalutation(),
            'title' => $user->getTitle(),
            'firstname' => $user->getFirstname(),
            'lastname' => $user->getLastname(),
            'email' => $user->getEmail(),
            'phone_country' => $user->getPhoneCountry(),
            'phone' => $user->getPhone(),
            'mobile_country' => $user->getMobileCountry(),
            'mobile' => $user->getMobile(),
            'fax_country' => $user->getFaxCountry(),
            'fax' => $user->getFax(),
        ];
    }
}
