<?php

namespace PassportBundle\Repository;

use Pimcore\Model\DataObject\Application;

interface ApplicationRepositoryInterface
{
    public function getById(int $id): ?Application;

    public function getBySystemId(string $systemId): ?Application;
}
