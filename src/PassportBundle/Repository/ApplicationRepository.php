<?php

namespace PassportBundle\Repository;

use Pimcore\Model\DataObject;

class ApplicationRepository implements ApplicationRepositoryInterface
{
    /**
     * @param int $id
     * @return DataObject\AbstractObject|DataObject\Application|DataObject\Concrete|null
     */
    public function getById(int $id): ?DataObject\Application
    {
        return DataObject\Application::getById($id);
    }

    /**
     * @param string $systemId
     * @return DataObject\Application|DataObject\Application\Listing
     */
    public function getBySystemId(string $systemId): ?DataObject\Application
    {
        return DataObject\Application::getBySystemId($systemId, 1);
    }
}
