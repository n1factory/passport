<?php

namespace PassportBundle\Controller;

use PassportBundle\Form\PasswordFormType;
use PassportBundle\Form\UserFormType;
use PassportBundle\Library\Helpers\SidebarNavigationBuilder;
use PassportBundle\Service\Sso\Session;
use PassportBundle\Service\Sso\SsoService;
use PassportBundle\Transformer\CustomerTransformerInterface;
use Pimcore\Controller\Configuration\ResponseHeader;
use Pimcore\Model\DataObject;
use Pimcore\Tool\Console;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class PassportController
 * @package Factory\UserManagementBundle\Controller
 *
 * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
 * @ResponseHeader("Cache-Control", values={"no-store"})
 */
class PassportController extends BaseController
{
    public const USER_AVATAR_FOLDER = 'user-avatars';

    /** @var EncoderFactoryInterface  */
    private $encoderFactory;

    /**
     * @var Session
     */
    protected $sessionService;

    /** @var TranslatorInterface $translator */
    protected $translator;

    /** @var SsoService $ssoService */
    protected $ssoService;

    /** @var SidebarNavigationBuilder $sidebarNavigationBuilder */
    protected $sidebarNavigationBuilder;

    /**
     * @var ParameterBagInterface
     */
    protected $parameterBag;

    /**
     * ProfileController constructor.
     * @param EncoderFactoryInterface $encoderFactory
     * @param Session $sessionService
     * @param SsoService $ssoService
     * @param TranslatorInterface $translator
     */
    public function __construct(
        EncoderFactoryInterface $encoderFactory,
        Session $sessionService,
        SsoService $ssoService,
        TranslatorInterface $translator,
        SidebarNavigationBuilder $sidebarNavigationBuilder,
        ParameterBagInterface $parameterBag
    ) {
        $this->encoderFactory = $encoderFactory;
        $this->sessionService = $sessionService;
        $this->translator = $translator;
        $this->ssoService = $ssoService;
        $this->sidebarNavigationBuilder = $sidebarNavigationBuilder;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @Route("/", name="index")
     * Redirect to dashboard if we're on Passport root document
     */
    public function indexAction(Request $request): RedirectResponse
    {
        return $this->redirectToRoute('passport-applications');
    }

    /**
     * @Route("dashboard", name="dashboard")
     * @todo Add data for dashboard view
     * @param Request $request
     * @throws \Exception
     */
    public function dashboardAction(
        Request $request
    ) {
        $locale = $request->getLocale();

        return $this->renderTemplate('@PassportBundle/Resources/views/Passport/dashboard.html.twig', [
            'locale' => $locale,
            'userData' => $this->getUserData(),
            'navigationData' => $this->sidebarNavigationBuilder->buildNavigation(),
        ]);
    }

    /**
     * @Route("user-profile", name="passport-user-profile")
     * @param Request $request
     * @param CustomerTransformerInterface $customerMapper
     * @throws \Exception
     */
    public function userProfileAction(
        Request $request,
        CustomerTransformerInterface $customerMapper
    ) {
        $locale = $request->getLocale();
        $user = $this->getUser();

        if ($user instanceof DataObject\Customer) {
            $userFormData = $customerMapper->transformForForm($user, $locale);
        }

        $userFormData['sso_site_link'] = $this->ssoService->getApplicationLinkById($request->get('site_id'));

        $userForm = $this->createForm(
            UserFormType::class,
            $userFormData,
            [
                'action' => $this->generateUrl('save_user'),
            ]
        );

        $passwordForm = $this->createForm(
            PasswordFormType::class,
            [],
            [
                'action' => $this->generateUrl('user_change_password')
            ]
        );

        return $this->renderTemplate('@PassportBundle/Resources/views/Passport/userProfile.html.twig', [
            'locale' => $locale,
            'userData' => $this->getUserData(),
            'userForm' => $userForm->createView(),
            'passwordForm' => $passwordForm->createView(),
            'navigationData' => $this->sidebarNavigationBuilder->buildNavigation(),
        ]);
    }

    /**
     * @Route("/user/save", name="save_user")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function saveUser(Request $request)
    {
        /** @var DataObject\Customer $customer */
        $customer = $this->getUser();

        $form = $this->createForm(UserFormType::class);

        $errors = [];

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $errors = $this->getFormErrors($form);
            } else {
                try {
                    $deleteImage = $request->get('delete_image');

                    $formData = $form->getData();
                    $imageFile = $formData['avatar'];

                    $customer->setSalutation($formData['salutation'])
                        ->setTitle($formData['title'])
                        ->setFirstname($formData['firstname'])
                        ->setLastname($formData['lastname'])
                        ->setPhoneCountry($formData['phone_country'])
                        ->setPhone($formData['phone'])
                        ->setMobileCountry($formData['mobile_country'])
                        ->setMobile($formData['mobile'])
                        ->setFaxCountry($formData['fax_country'])
                        ->setFax($formData['fax']);

                    if ($deleteImage) {
                        $oldAvatar = $customer->getAvatar();

                        if ($oldAvatar) {
                            $oldAvatar->delete();
                        }

                        $customer->setAvatar(null);
                    }

                    if ($formData['avatar']) {
                        if ($customer->getAvatar()) {
                            $oldAvatar = $customer->getAvatar();
                            $oldAvatar->delete();
                        }

                        $avatar = new \Pimcore\Model\Asset();
                        $avatar->setFilename('user_avatar_' . $customer->getId() . '.png');
                        $avatar->setData(file_get_contents($imageFile));

                        $avatarFolderPath = '/' . self::USER_AVATAR_FOLDER;
                        $avatarFolder = \Pimcore\Model\Asset::getByPath($avatarFolderPath);

                        if (!$avatarFolder) {
                            $avatarFolder = \Pimcore\Model\Asset\Service::createFolderByPath($avatarFolderPath);
                        }

                        $avatar->setParent($avatarFolder);
                        $avatar->save();

                        $customer->setAvatar($avatar);
                    }

                    $customer->save();

                    // Run command for updating users on other sites in background
                    Console::runPhpScriptInBackground(
                        realpath(PIMCORE_PROJECT_ROOT . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'console'),
                        "api:update-users --user-id=" . $customer->getId()
                    );

                    $redirectLink = '';
                    if ($formData['sso_site_link']) {
                        $redirectLink = $formData['sso_site_link'];
                    }

                    return new JsonResponse([
                        'messages' => [
                            'success_message' => $this->translator->trans('user-profile-page.update.success-message'),
                        ],
                        'redirect_link' => $redirectLink,
                    ], Response::HTTP_OK);
                } catch (Exception $e) {
                    $errors[] = $e->getMessage();
                }
            }
        }

        return new JsonResponse([
            'data' => [
                'errors' => $errors
            ],
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Return view with user applications (licences for companies and organisations) list
     *
     * @Route("applications", name="passport-applications")
     * @param Request $request
     * @throws \Exception
     */
    public function applicationsAction(Request $request)
    {
        $user = $this->getUser();
        $locale = $request->getLocale();

        if ($user instanceof DataObject\Customer) {
            $data['applications'] = $this->sessionService->getAllowedUserApplications();
        }

        return $this->renderTemplate('@PassportBundle/Resources/views/Passport/applications.html.twig', [
            'data' => $data,
            'locale' => $locale,
            'userData' => $this->getUserData(),
            'navigationData' => $this->sidebarNavigationBuilder->buildNavigation(),
        ]);
    }

    /**
     * Send email for changing users password
     *
     * @Route("/user-profile/request-change-password", name="user-request-change-password")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function sendChangePasswordEmail(Request $request)
    {
        $locale = $request->get('locale');

        /** @var DataObject\Customer $user */
        $user = $this->getUser();

        if ($user) {
            $tokenService = $this->get('userauthentication.user.token');

            $token = $tokenService->getTokenByCustomer($user);
        }

        if (!$token) {
            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
            ], Response::HTTP_BAD_REQUEST);
        }

        $mailerService = $this->get('userauthentication.mailer');

        if ($user->getN1User()) {
            $mailerService->useConfig('configuration_id');
        }

        $mailerService->sendChangePasswordMail($token, $user->getEmail(), $locale);

        return new JsonResponse([
            'message' => Response::$statusTexts[Response::HTTP_OK],
        ], Response::HTTP_OK);
    }

    /**
     * Return view with change password form, handles that form
     *
     * @Route("/user-profile/change-password", name="user_change_password", methods={ "POST" })
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function changePasswordAction(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(PasswordFormType::class);
        $userEncoder = $this->encoderFactory->getEncoder($user);

        $errors = [];
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $errors = $this->getFormErrors($form);
            } else {
                $formData = $form->getData();

                $redirectLink = '';
                if ($formData['sso_site_link']) {
                    $redirectLink = $formData['sso_site_link'];
                }

                try {
                    $oldPassword = $formData['current_password'];
                    $newPassword = $formData['password'];

                    if ($oldPassword !== $newPassword) {
                        $validOldPassword = $userEncoder->isPasswordValid($user->getPassword(), $oldPassword, $user->getUsername());

                        if (!$validOldPassword) {
                            return new JsonResponse([
                                'data' => [
                                    'errors' => [
                                        'current_password' => [
                                            0 => $this->translator->trans('form.validation.invalid-current-password'),
                                        ],
                                    ]
                                ]
                            ], Response::HTTP_BAD_REQUEST);
                        }

                        $hashedPassword = $userEncoder->encodePassword($newPassword, $user->getUsername());

                        $user->setPassword($hashedPassword);

                        $user->save();

                        return new JsonResponse([
                            'messages' => [
                                'success_message' => $this->translator->trans('user-profile-page.update.success-message'),
                            ],
                            'redirect_link' => $redirectLink,
                        ], Response::HTTP_OK);
                    }

                    return new JsonResponse([
                        'messages' => [
                            'success_message' => $this->translator->trans('user-profile-page.update.success-message'),
                        ],
                        'redirect_link' => $redirectLink,
                    ], Response::HTTP_OK);
                } catch(\Exception $e) {
                    $errors[] = $e->getMessage();
                }
            }
        }

        return new JsonResponse([
            'data' => [
                'errors' => $errors
            ],
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Fetch formatted data for user
     * [Header info]
     * @return arry
     */
    protected function getUserData() {
        $user = $this->getUser();

        if ($user && get_class($user) == 'Pimcore\Bundle\AdminBundle\Security\User\User') {
            $user = $user->getUser();
        }

        if ($user) {
            $userData = [
                'firstname' => $user->getFirstname(),
                'lastname' => $user->getLastname(),
                'mail' => $user->getEmail(),
                'company' => 'Musterfirma'
            ];
        } else {
            $userData = [
                'firstname' => 'Not',
                'lastname' => 'Logged In',
                'mail' => 'not-logged-in@valid.com',
                'company' => 'n07l0663d1n ©'
            ];
        }

        return $userData;
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    protected function getFormErrors(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors(true) as $formError) {
            $errorCause = $formError->getCause();

            if ($errorCause instanceof CsrfToken) {
                $errors['csrf_token'][] = $formError->getMessage();
                continue;
            }

            $fieldName = $formError->getOrigin()->getName();

            $parent = $formError->getOrigin()->getParent();

            if ($parent) {
                $parentName = $formError->getOrigin()->getParent()->getName();

                // Some fields have parent fields (e.g. Repeated type field)
                if ($parentName) {
                    $fieldName = "$parentName\[$fieldName\]";
                }
            }

            $errors[$fieldName][] = $formError->getMessage();
        }

        return $errors;
    }
}
