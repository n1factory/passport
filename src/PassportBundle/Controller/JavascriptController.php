<?php

namespace PassportBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class JavascriptController extends FrontendController {

    const TRANSLATIONS_TABLE = "translations_website";

    /**
     * @Route("/{_locale}/javascript/frontend")
     */
    public function frontendAction(Request $request)
    {
        header('Content-Type: text/javascript');

        $content = "var cembrit = cembrit ? cembrit : {};";

        $locale = $request->get("_locale");

        $translations = $this->getTranslations($locale);

        $language = explode("_", $locale)[0];

        $localeData = [
            "locale" => $locale,
            "language" => $language,
            "translations" => $translations
        ];

        $regionsListing = new \Pimcore\Model\DataObject\Region\Listing();

        $regionNames = [];

        foreach ($regionsListing as $region) {
            $regionNames[$region->getId()] = $region->getName($locale);
        }

        \LayoutBundle\Library\Frontend\Javascript\Data::add("i18n", $localeData);

        \LayoutBundle\Library\Frontend\Javascript\Data::add("regions", $regionNames);

        $config = \LayoutBundle\Library\Frontend\Javascript\Data::get(false);

        $content .= "cembrit.i18n = " . json_encode($config["i18n"]);

        unset($config["i18n"]);

        $content .= "; cembrit.data = " . json_encode($config);

        die($content);
    }

    /**
     *
     * @param string $locale
     * @return array
     */
    private function getTranslations($locale)
    {
        $db = \Pimcore::getContainer()->get("Pimcore\Db\Connection");

        $query = "SELECT * FROM " . self::TRANSLATIONS_TABLE . " WHERE language = ?";

        $translations = $db->fetchAll($query, [$locale]);

        $mappedTranslations = [];

        if ($translations !== null && $translations !== false && count($translations) > 0) {
            foreach ($translations as $translationResult) {
                $mappedTranslations[$translationResult["key"]] = $translationResult["text"];
            }
        }

        return $mappedTranslations;
    }
}
