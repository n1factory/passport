<?php

namespace PassportBundle\Controller;

use Carbon\Carbon;
use PassportBundle\Service\BiApiService;
use PassportBundle\Service\BiApiTokenService;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BiApiController extends FrontendController
{
    /** @var BiApiService */
    private $biApiService;

    /** @var BiApiTokenService */
    private $biApiTokenService;

    public function __construct(
        BiApiService $biApiService,
        BiApiTokenService $biApiTokenService
    ) {
        $this->biApiService = $biApiService;
        $this->biApiTokenService = $biApiTokenService;
    }

    /**
     * @Route("/api/get-user-data", methods={ "GET" })
     */
    public function getUserData(Request $request): \Symfony\Component\HttpFoundation\JsonResponse
    {
        if (!$token = $this->biApiTokenService->authorizeRequest($request)) {
            return $this->json([
                'message' => 'Invalid token.',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $userData = $this->biApiService->getUserData($this->biApiTokenService->getPayload($token));

        return $this->json($userData);
    }

    /**
     * @Route("/api/fake-token", methods={ "GET" })
     */
    public function fakeToken(Request $request)
    {
        return $this->json($this->biApiTokenService->generate([
            'email' => $request->get('email'),
            'iat' => Carbon::now()->timestamp,
        ]));
    }
}
