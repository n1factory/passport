<?php

namespace PassportBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;

class EmailController extends FrontendController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function defaultAction(Request $request)
    {
        $hostUrl = $request->isSecure() ? \Pimcore\Tool::getHostUrl('https') : \Pimcore\Tool::getHostUrl();

        return $this->renderTemplate('@PassportBundle/Resources/views/Email/default.html.twig', [
            'hostUrl' => $hostUrl
        ]);
    }
}
