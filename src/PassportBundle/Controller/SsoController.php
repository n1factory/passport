<?php

namespace PassportBundle\Controller;

use PassportBundle\Service\Sso\Session;
use Pimcore\Config;
use Pimcore\Controller\FrontendController;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SsoController extends FrontendController
{
    /**
     * @var Session
     */
    private $sessionService;

    public function __construct(Session $sessionService, SessionStorageInterface $sessionStorage)
    {
        $this->sessionService = $sessionService;
    }

    /**
     * @Route("/sso/user-info", name="sso-user-info", methods = {"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function userInfoAction(Request $request): JsonResponse
    {
        error_log("ACTION: userInfoAction");

        try {
            if (!($request->get("system-id") && $request->get("token") && $request->get("checksum"))) {
                return new JsonResponse('{}', 403);
            }

            if ($this->sessionService->checksumIsValid($request->get("system-id"), $request->get("token"), $request->get("checksum")) !== true) {
                return new JsonResponse('{}', 403);
            }

            $sessionData = $this->sessionService->getByExternalSessionIdAndSystemId($request->get("token"), $request->get("system-id"));

            if ($sessionData && is_array($sessionData) && is_string($sessionData['user'])) {
                return new JsonResponse(json_decode($sessionData['user']));
            } else {
                return new JsonResponse([], 403);
            }
        } catch (\Exception $e) {
            error_log("Error: " . $e->getMessage());

            return new JsonResponse(["error" => "See logs for more details."], 400);
        }
    }

    /**
     * @Route("/sso/user-logout")
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @return RedirectResponse
     */
    public function userLogoutAction(Request $request, TokenStorageInterface $tokenStorage): RedirectResponse
    {
        if (!($request->get("system-id") && $request->get("token") && $request->get("checksum"))) {
            Simple::log('logout', 'Invalid request: ' . $request->getRequestUri());
            $this->redirect($request->get('return') ?: '/');
        }

        if ($this->sessionService->checksumIsValid($request->get("system-id"), $request->get("token"), $request->get("checksum")) !== true) {
            Simple::log('logout', 'Invalid checksum: ' . $request->getRequestUri());
            $this->redirect($request->get('return') ?: '/');
        }

        $externalSessionData = $this->sessionService->getByExternalSessionIdAndSystemId($request->get("token"), $request->get("system-id"));

        if($externalSessionData && $externalSessionData['internal_session']) {
            // logout user from all systems
            $this->sessionService->deleteDataByInternalSessionId($externalSessionData['internal_session']);
        }

        $tokenStorage->setToken(null);
        $request->getSession()->clear();

        return $this->redirect($request->get('return') ?: '/');
    }

    /**
     * @Route("/sso/application-login/{systemId}", name="sso_application_login")
     * @param Request $request
     * @throws \Exception
     */
    public function logInIntoApplication($systemId, Request $request, Session $sessionService)
    {
        $application = DataObject\Application::getBySystemId($systemId, 1);

        if(!$sessionService->appIsAllowedForUser($application->getSystemId())) {
            throw new \Exception('Application not allowed');
        }

        $redirectUrl = $sessionService->logInApplication($application, $request->getSession()->getId());

        return new RedirectResponse($redirectUrl);
    }

    /**
     * @Route("api/sso/application-login", name="api_sso_application_login")
     * @param Request $request
     * @throws \Exception
     */
    public function logInIntoApplicationViaAnotherApplication(Request $request, Session $sessionService)
    {
        $systemId = $request->get('system-id');
        $token = $request->get('token');
        $checksum = $request->get('checksum');

        if (hash('sha512', $systemId . $token) !== $checksum) {
            return new RedirectResponse($request->server->get('HTTP_REFERER'));
        }

        $application = DataObject\Application::getBySystemId($systemId, 1);

        $user = $this->getUser();

        if(!$user || !$sessionService->appIsAllowedForUser($application->getSystemId())) {
            return new RedirectResponse($request->server->get('HTTP_REFERER'));
        }

        $redirectUrl = $sessionService->logInApplication($application, $request->getSession()->getId());

        return new RedirectResponse($redirectUrl);
    }
}
