<?php

namespace PassportBundle\Controller;

use PassportBundle\Repository\ApplicationRepositoryInterface;
use PassportBundle\Service\ApplicationInvitationService;
use PassportBundle\Service\UserService;
use Pimcore\Controller\FrontendController;
use Pimcore\Log\Simple;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

class UserApiController extends FrontendController
{
    /** @var ApplicationRepositoryInterface $applicationRepository */
    private $applicationRepository;

    /**
     * UserApiController constructor.
     * @param ApplicationRepositoryInterface $applicationRepository
     */
    public function __construct(ApplicationRepositoryInterface $applicationRepository)
    {
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * @Route("api/user/remove-application", methods={ "POST" })
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function removeApplicationFromUser(Request $request, UserService $userService)
    {
        if (!$this->validateApiRequest($request)) {
            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED]
            ], Response::HTTP_UNAUTHORIZED);
        }

        $requestParamsValidation = [
            'application_id' => [new Assert\Required(), new Assert\NotBlank()],
            'user_email' => [new Assert\Required(), new Assert\NotBlank()],
        ];

        $errors = $this->validateApiRequestParams($request, $requestParamsValidation);

        if (count($errors)) {
            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                'errors' => $errors,
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            $userEmail = $request->get('user_email');
            $applicationId = $request->get('application_id');

            $userService->removeApplicationFromUser($userEmail, $applicationId);

            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_OK],
            ]);
        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("api/user/assign-application", methods={ "POST" })
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function assignApplicationToUser(Request $request, UserService $userService)
    {
        if (!$this->validateApiRequest($request)) {
            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED]
            ], Response::HTTP_UNAUTHORIZED);
        }

        $requestParamsValidation = [
            'application_id' => [new Assert\Required(), new Assert\NotBlank()],
            'user_email' => [new Assert\Required(), new Assert\NotBlank()],
        ];

        $errors = $this->validateApiRequestParams($request, $requestParamsValidation);

        if (count($errors)) {
            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                'errors' => $errors,
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            $userEmail = $request->get('user_email');
            $applicationId = $request->get('application_id');

            $userService->assignApplicationToUser($userEmail, $applicationId);

            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_OK],
            ]);
        } catch (\Throwable $e) {
            return new JsonResponse([
                'message' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("api/user/handle-invite", methods={ "POST" })
     * @param Request $request
     * @param ApplicationInvitationService $invitationService
     * @return JsonResponse
     */
    public function handleInvitation(Request $request, ApplicationInvitationService $invitationService)
    {
        if (!$this->validateApiRequest($request)) {
            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED]
            ], Response::HTTP_UNAUTHORIZED);
        }

        $requestParamsValidation = [
            'first_name' => [new Assert\NotBlank()],
            'last_name' => [new Assert\NotBlank()],
            'email' => [new Assert\NotBlank()],
            'application_id' => [new Assert\NotBlank()],
            'role' => [new Assert\NotBlank()],
            'sales_office' => [new Assert\NotBlank()],
            'organisation_id' => [new Assert\NotBlank()],
        ];

        $errors = $this->validateApiRequestParams($request, $requestParamsValidation);

        if (count($errors)) {
            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                'errors' => $errors,
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            $invitationService->handleInvitationForApplication($request->request->all());

            return new JsonResponse([
                'message' => Response::$statusTexts[Response::HTTP_OK],
            ]);
        } catch (\Exception $e) {
            Simple::log('user_invite', $e->getMessage());
            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function validateApiRequest(Request $request)
    {
        if (empty($request->get('application_id'))) {
            return false;
        }

        $application = $this->applicationRepository->getBySystemId($request->get('application_id'));
        $secret = $application->getSecret();

        $checksum = $request->get('checksum');
        $token = $request->get('token');

        $newChecksum = hash('sha512', $token . $secret);

        if ($checksum !== $newChecksum) {
            return false;
        }

        return true;
    }

    private function validateApiRequestParams(Request $request, array $paramConstraints)
    {
        $validator = Validation::createValidator();

        $constraint = new Assert\Collection($paramConstraints);

        $requestParams = [];

        foreach (array_keys($paramConstraints) as $paramName) {
            $requestParams[$paramName] = $request->get($paramName);
        }

        $violations = $validator->validate($requestParams, $constraint);

        $errors = [];

        if ($violations->count()) {
            foreach ($violations as $violation) {
                $propertyName = trim($violation->getPropertyPath(), '[]');
                $errors[$propertyName][] = $violation->getMessage();
            }
        }

        return $errors;
    }
}
