<?php

namespace PassportBundle\Controller;

use PassportBundle\Form\LoginFormType;
use PassportBundle\Service\ConfigService;
use PassportBundle\Service\Sso\Session;
use Pimcore\Controller\FrontendController;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class MobileLoginController extends FrontendController
{
    /** @var TranslatorInterface */
    protected $translator;

    /** @var Session */
    private $sessionService;

    public function __construct(
        TranslatorInterface $translator,
        Session $session
    ) {
        $this->translator = $translator;
        $this->sessionService = $session;
    }

    /**
     * Set twig as rendering template engine
     *
     * @param FilterControllerEvent $event
     * @return void
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $this->setViewAutoRender($event->getRequest(), true, 'twig');
    }

    /**
     * Security will redirect here, since UserAuthenticationBundle\Library\Security\Authenticator have "start" method
     * => login_page document needs to be set in Configuration object
     *
     * @Route("mobile-login", name="mobile_passport_login")
     *
     * @param Request $request
     * @param UserInterface|null $user
     * @param AuthenticationUtils $authenticationUtils
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function loginAction(
        Request $request,
        UserInterface $user = null,
        AuthenticationUtils $authenticationUtils,
        ConfigService $configService
    ) {
        $locale = $request->getLocale();
        $shortLocale = explode('_', $locale)[0];

        // Redirect user to user profile page if he's already logged in
        if ($user && $this->isGranted('ROLE_USER')) {
            $redirectLink = $request->get('return');

            // Redirect
            if ($redirectLink) {
                return $this->redirect($redirectLink);
            }
        }

        /** @var DataObject\Configuration $configuration */
        $configuration = $configService->getConfiguration();

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $formData = [
            '_username' => $lastUsername,
            'email' => $request->get('email')
        ];

        $loginLink = $this->generateUrl('mobile_passport_login');

        if($request->query->all()) {
            $loginLink .= "?" . http_build_query($request->query->all());
        }

        $passwordForgottenLink = $this->generateUrl('password-forgot', ['locale' => $shortLocale]);

        $form = $this->createForm(LoginFormType::class, $formData, [
            'action' => $loginLink,
            'attr' => [
                'id' => 'login-form',
                'class' => 'login-form',
            ],
        ]);

        if ($error and $request->getMethod() === Request::METHOD_POST) {
            return new JsonResponse(['error' => $error], 422);
        }

        $recaptchaSiteKey = WebsiteSetting::getByName('recaptcha_site_key');

        if (!empty($recaptchaSiteKey)) {
            $this->view->recaptcha_site_key = $recaptchaSiteKey->getData();
        }


        if ($request->query->has('authenticated') && $request->query->get('authenticated') == "0") {
            $error = $this->translator->trans('login.authentication-failure');
        }

        $this->view->error = $error;
        $this->view->form = $form->createView();
        $this->view->passwordForgotLink = $passwordForgottenLink;
    }

    /**
     * @Route("mobile-exit", name="mobile_passport_logout")
     */
    public function logout(Request $request, Session $sessionService, TokenStorageInterface $tokenStorage)
    {
        if (!($request->get("system-id") && $request->get("token") && $request->get("checksum"))) {
            Simple::log('logout', 'Invalid request: ' . $request->getRequestUri());
            $this->redirect($request->get('return') ?: '/');
        }

        if ($this->sessionService->checksumIsValid($request->get("system-id"), $request->get("token"), $request->get("checksum")) !== true) {
            Simple::log('logout', 'Invalid checksum: ' . $request->getRequestUri());
            $this->redirect($request->get('return') ?: '/');
        }

        $externalSessionData = $sessionService->getByExternalSessionIdAndSystemId($request->get("token"), $request->get("system-id"));

        if($externalSessionData && $externalSessionData['internal_session']) {
            // logout user from all systems
            $sessionService->deleteDataByInternalSessionId($externalSessionData['internal_session']);
        }

        $tokenStorage->setToken(null);
        $request->getSession()->clear();

        return $this->redirect($request->get('return') ?: '/');
    }
}
