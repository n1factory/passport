<?php

namespace PassportBundle\Controller;


use CustomerManagementFrameworkBundle\CustomerProvider\CustomerProviderInterface;
use CustomerManagementFrameworkBundle\Model\CustomerInterface;
use PassportBundle\Form\LoginFormType;
use PassportBundle\Form\PasswordForgotType;
use PassportBundle\Form\UserRegistration;
use PassportBundle\Helper\Server;
use PassportBundle\Service\ApplicationInvitationService;
use PassportBundle\Service\EmailService;
use PassportBundle\Service\PasswordService;
use PassportBundle\Service\RecaptchaService;
use PassportBundle\Service\RegistrationFormHandler;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Customer;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Controller\Configuration\ResponseHeader;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;
use Pimcore\Model\Site;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @ResponseHeader("Cache-Control", values={"no-store"})
 */
class AuthController extends FrontendController
{
    private $translator;
    private $emailService;

    public function __construct(
        TranslatorInterface $translator,
        EmailService $emailService
    ) {
        $this->translator = $translator;
        $this->emailService = $emailService;
    }

    /**
     * Set twig as rendering template engine
     *
     * @param FilterControllerEvent $event
     * @return void
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $this->setViewAutoRender($event->getRequest(), true, 'twig');
    }

    /**
     * Security will redirect here, since UserAuthenticationBundle\Library\Security\Authenticator have "start" method
     * => login_page document needs to be set in Configuration object
     *
     * @Route("login", name="login")
     *
     * @param Request $request
     * @param UserInterface|null $user
     * @param AuthenticationUtils $authenticationUtils
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function loginAction(
        Request $request,
        UserInterface $user = null,
        AuthenticationUtils $authenticationUtils
    ) {
        $locale = $request->getLocale() ?: \Pimcore\Tool::getDefaultLanguage();
        $shortLocale = explode('_', $locale)[0];

        // Redirect user to user profile page if he's already logged in
        if ($user && $this->isGranted('ROLE_USER')) {

            if ($user->getPasswordRecoveryRequested()) {
                // @Todo handle password change request
                $redirectLink = $this->generateUrl('index');
            } else {
                $redirectLink = $this->generateUrl('index');
            }

            // Redirect
            return $this->redirect($redirectLink);
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $formData = [
            '_username' => $lastUsername,
            'email' => $request->get('email')
        ];

        $loginLink = $this->generateUrl('login');

        if($request->query->all()) {
            $loginLink .= "?" . http_build_query($request->query->all());
        }

        $passwordForgottenLink = $this->generateUrl('password-forgot', ['locale' => $shortLocale]);

        $form = $this->createForm(LoginFormType::class, $formData, [
            'action' => $loginLink,
            'attr' => [
                'id' => 'login-form',
                'class' => 'login-form',
            ],
        ]);

        if ($error and $request->getMethod() === Request::METHOD_POST) {
            return new JsonResponse(['error' => $error], 422);
        }

        $recaptchaSiteKey = WebsiteSetting::getByName('recaptcha_site_key');

        if (!empty($recaptchaSiteKey)) {
            $this->view->recaptcha_site_key = $recaptchaSiteKey->getData();
        }

        $showRequestInvite = false;

        if (Site::isSiteRequest() && Site::getCurrentSite()->getRootId() == $this->getParameter('n1.homepage.n1-highway.id')) {
            $showRequestInvite = false;
        }

        //Get login links
        $loginPageLinksData = [];

        if ($request->query->has('authenticated') && $request->query->get('authenticated') == "0") {
            $error = $this->translator->trans('login.authentication-failure');
        }

        $this->view->error = $error;

        $this->view->form = $form->createView();
        $this->view->passwordForgotLink = $passwordForgottenLink;
        $this->view->userRegistrationLink = '';

        $this->view->showRequestInvite = $showRequestInvite;
        $this->view->loginPageLinksData = $loginPageLinksData;
        $this->view->alreadyRegistered = $request->get('already_registered', 0);
    }

    /**
     * Dummy empty route for symfony "logout magic"
     * @Route("logout", name="logout")
     *
     * @param Request $request
     */
    public function logoutAction(Request $request)
    {

    }

    /**
     * @Route("{locale}/password-forgot", name="password-forgot", methods={"GET", "POST"}, requirements={ "locale"="de|en" })
     *
     * @param Request $request
     * @param PasswordService $passwordService
     * @return RedirectResponse
     * @throws \Exception
     */
    public function passwordForgotAction(Request $request, PasswordService $passwordService)
    {
        $form = $this->createForm(PasswordForgotType::class, [], [
            'attr' => [
                'id' => 'login-form',
                'class' => 'login-form',
            ],
            'action' => $request->attributes->get('contentDocument')->getFullPath()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                return new RedirectResponse($request->getRequestUri() . "?error_code=4");
            }

            $formData = $form->getData();

            $email = $formData['_email'];

            $request->getSession()->set("email", $email);

            $user = null;

            $user = DataObject\Customer::getByEmail($email, 1);

            if (!$user instanceof DataObject\Customer) {
                return new RedirectResponse($request->getRequestUri() . "?error_code=4");
            }

            if (!$user->getPasswordRecoveryRequested()) {
                $user->setPasswordRecoveryRequested(true)->save();
            }

            if ($user instanceof DataObject\Customer) {
                $tempPassword = $passwordService->generateRandomPassword($user);
                $this->emailService->sendPasswordRecoveryEmail($user, $tempPassword);
            }

            return new RedirectResponse($request->getRequestUri() . "?success=1");
        }

        $success = 0;
        $error = '';

        $this->view->form = $form->createView();

        if ($request->get("success")) {
            $success = 1;
        }

        if ($request->get("error_code")) {
            $error = $this->getErrorMessageByErrorCode($request->get("error_code"), $request->getSession()->get("email"));
        }

        $this->view->success = $success;
        $this->view->error = $success;

        $this->view->email = $request->getSession()->get("email");
        $this->view->lang = explode('_', $request->getLocale())[0];

        //WIP:
//         return $this->renderTemplate("@UserAuthenticationBundle/Resources/views/UserAuthentication/passwordForgot.html.twig");
    }

    /**
     * If registration is called with a registration key, the key will be used to look for an existing OAuth token in
     * the session. This OAuth token will be used to fetch user info which can be used to pre-populate the form and to
     * link a SSO identity to the created customer object.
     *
     * This could be further separated into services, but was kept as single method for demonstration purposes as the
     * registration process is different on every project.
     *
     * @Route("/register", name="register")
     * @param Request $request
     * @param CustomerProviderInterface $customerProvider
     * @param RegistrationFormHandler $registrationFormHandler
     * @param UserInterface|null $user
     * @return RedirectResponse|Response|null
     */
    public function registerAction(
        Request $request,
        CustomerProviderInterface $customerProvider,
        RegistrationFormHandler $registrationFormHandler,
        RecaptchaService $recaptchaService,
        ApplicationInvitationService $applicationInvitationService,
        UserInterface $user = null
    ) {
        $locale = $request->getLocale();

        $userRegisterPage = $this->generateUrl('register');

        if ($user && $this->isGranted('ROLE_USER')) {
            $targetLink = $this->generateUrl('dashboard');

            return $this->redirect($targetLink);
        }

        $registrationKey = $request->get('registrationKey');

        $hidePassword = false;

        $formData["translator"] = $this->translator;

        $formData['user_data'] = [
            'email' => $request->get('email') ?: '',
            'firstname' => $request->get('firstname') ?: '',
            'lastname' => $request->get('lastname') ?: ''
        ];

        if ($request->get('_tok')) {
            $formData['initialTokenValue'] = $request->get('_tok');
        }

        // Only if _application_token is set in query render application_token field in form
        if ($request->get('_application_token')) {
            $invitation = $applicationInvitationService->getByHashedToken($request->get('_application_token'));

            if ($invitation) {
                $formData['application_token'] = $invitation->getInvitationTokenKey();

                $userInDb = Customer::getByEmail($invitation->getEmail(), 1);

                if ($userInDb) {
                    return $this->redirect($this->generateUrl('login', ['already_registered' => 1]));
                }

                $formData['user_data'] = [
                    'email' => $invitation->getEmail(),
                    'firstname' => $invitation->getFirstName(),
                    'lastname' => $invitation->getLastName(),
                ];
            }
        }

        // build the registration form and pre-fill it with customer data
        $form = $this->createForm(UserRegistration::class, $formData, [
            'attr' => [
                'class' => 'register-form',
            ],
            'action' => $request->attributes->get('contentDocument')->getFullPath()
        ]);

        $customerListing = new DataObject\Customer\Listing();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $formData = $form->getData();

            $email = $formData['register_email'];

            $initialTokenString = $request->get('initialToken');

            // user was not invited but wants to do "initial" registration on page
            $customerListing->addConditionParam("email = '$email'");

            //user with same email already exists, invalidate form by adding error
            if ($customerListing->count() > 0) {
                $errorMessage = $this->translator->trans("user.registration.user-exists");

                $form->addError(new FormError($errorMessage));
            }

            $password = $request->get('register_password');
            $passwordRepeat = $request->get('register_password_repeat');

            if ($password !== $passwordRepeat) {
                $errorMessage = $this->translator->trans('registration.passwords_dont_match');

                $form->addError(new FormError($errorMessage));
            }
        }

        // create a new, empty customer instance
        /** @var CustomerInterface|\Pimcore\Model\DataObject\Customer $customer */
        $customer = $customerProvider->create();

        // the registration form handler is just a utility class to map pimcore object data to form
        // and vice versa.
        $registrationFormHandler->buildFormData($customer);

        $errors = [];

        if ($form->isSubmitted() && $form->isValid() && $recaptchaService->validateResponse()) {

            $registrationFormHandler->updateCustomerFromForm($customer, $form);

            $formData = $form->getData();

            try {
                $customer->setNotifications(true)
                    ->setActive(true)
                    ->setPassword($request->get('register_password'))->save();

                $confirmationEmailUserData = [];

                $confirmationEmailUserData['firstname'] = $formData['register_firstname'];
                $confirmationEmailUserData['lastname'] = $formData['register_lastname'];
                $confirmationEmailUserData['email'] = $formData['register_email'];

                $this->emailService->sendUserRegistrationConfirmationMail($locale, $confirmationEmailUserData);

                return new RedirectResponse($userRegisterPage . "?user-registration=1");
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            foreach ($form->getErrors(true) as $error) {
                $errors[] = $error->getMessage();
            }
        }

        $recaptchaSiteKey = WebsiteSetting::getByName('recaptcha_site_key');

        if (!empty($recaptchaSiteKey)) {
            $this->view->recaptcha_site_key = $recaptchaSiteKey->getData();
        }

        $showRequestInvite = false;

        if (Site::isSiteRequest() && Site::getCurrentSite()->getRootId() == $this->getParameter('n1.homepage.n1-highway.id')) {
            $showRequestInvite = false;
        }

        $this->view->showRequestInvite = $showRequestInvite;
        $this->view->customer = $customer;
        $this->view->domain = Server::getBaseUrl();
        $this->view->form = $form->createView();
        $this->view->errors = $errors;
        $this->view->hideNav = true;
        $this->view->hideBreadcrumb = true;
        $this->view->hidePassword = $hidePassword;
        $this->view->userRegistered = $request->get("user-registration");
        $this->view->userRegistrationConfirmed = $request->get("registration-confirmed");
        $this->view->errorMessage = $this->getErrorMessageByErrorCode($request->get("error_code"));
        $this->view->registrationUrl = $this->generateUrl('register');
        $this->view->locale = $locale;
    }

    /**
     *
     * @param string $errorCode
     * @return string
     */
    private function getErrorMessageByErrorCode($errorCode, $data = null)
    {
        $errorMessages = [
            "1" => $this->translator->trans("token.is.invalid", [$data]),
            "2" => $this->translator->trans("customer.does.not.exist", [$data]),
            "3" => $this->translator->trans("customer.is.active", [$data]),
            "4" => $this->translator->trans("email.not.valid", [$data])
        ];

        return $errorMessages[$errorCode];
    }
}
