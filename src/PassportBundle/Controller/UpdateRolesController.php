<?php

namespace PassportBundle\Controller;

use PassportBundle\Service\ApplicationInvitationService;
use Pimcore\Controller\FrontendController;
use Pimcore\Log\Simple;
use Pimcore\Model\DataObject\Application;
use Pimcore\Model\DataObject\Customer;
use Pimcore\Model\DataObject\Fieldcollection\Data\AllowedApplication;
use Pimcore\Model\DataObject\Role;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UpdateRolesController extends FrontendController
{
    /**
     * @Route("api/user/update-roles", methods={ "POST" })
     * @param Request $request
     * @return JsonResponse
     */
    public function updateRoles(Request $request)
    {
        $systemId = $request->get('system_id');
        $usersData = $request->get('users');

        try {
            $this->updateUsersRoles($systemId, $usersData);
        } catch (\Throwable $e) {
            Simple::log('api', 'User roles update failed: ' . $e->getMessage());
        }
    }

    private function updateUsersRoles(string $systemId, array $usersData)
    {
        \Pimcore\Model\DataObject::setHideUnpublished(false);

        $application = Application::getBySystemId($systemId, 1);

        if (!$application instanceof Application) {
            return;
        }

        foreach ($usersData as $userData) {
            $user = Customer::getByEmail($userData['email'], 1);

            if (!$user) {
                continue;
            }

            $userApplications = $user->getUserApplications();

            if ($userApplications) {
                /** @var AllowedApplication $userApplication */
                foreach ($userApplications as $userApplication) {
                    if (
                        $userApplication->getApplication() &&
                        $userApplication->getApplication()->getId() == $application->getId()
                    ) {
                        $roleList = new Role\Listing();
                        $roleList->setLocale('de_DE');

                        $roleList->setCondition('o_parentId = ? AND RoleName = ?', [$application->getId(), $userData['role_name']]);
                        $role = $roleList->current();

                        if ($role instanceof Role) {
                            $userApplication->setRole($role);
                        }
                    }
                }
            }

            $user->save();
        }

        \Pimcore\Model\DataObject::setHideUnpublished(true);
    }
}
