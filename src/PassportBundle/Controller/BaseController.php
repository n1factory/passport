<?php

namespace PassportBundle\Controller;

use PassportBundle\Service\Sso\SsoService;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Routing\RouterInterface;

class BaseController extends FrontendController
{
    protected $filterOptionsFactory;
    protected $translator;
    protected $router;

    protected $ssoService;

    public function __construct(
        TranslatorInterface $translator,
        RouterInterface $router,
        RequestStack $requestStack,
        SsoService $ssoService
    ) {
        $this->translator = $translator;
        $this->router = $router;

        $this->ssoService = $ssoService;

        if($requestStack->getCurrentRequest()->get("pimcore_preview") === "true") {
            die("Preview Mode");
        }
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        parent::onKernelController($event);

        $this->ssoService->handleLoginStatus();
    }

    public function renderAccessDenied()
    {
        return new Response(Response::$statusTexts[Response::HTTP_FORBIDDEN], Response::HTTP_FORBIDDEN);
    }

    public function renderAccessDeniedJson () {
        return new JsonResponse([
            'error' => 'Access denied',
            'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
        ], Response::HTTP_BAD_REQUEST);
    }

    public function renderBadRequestJson () {
        return new JsonResponse([
            'error' => 'Access denied',
            'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
        ], Response::HTTP_BAD_REQUEST);
    }
}
