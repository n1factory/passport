<?php

namespace PassportBundle\Library\Helpers;

use Pimcore\Model\Site;
use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class SidebarNavigationBuilder
{
    /** @var RouterInterface $router */
    private $router;

    /** @var RequestStack $requestStack */
    protected $requestStack;

    public function __construct(
        RouterInterface $router,
        RequestStack $requestStack
    ) {
        $this->router = $router;
        $this->currentRoute = $requestStack->getCurrentRequest()->get('_route');
    }

    /**
     * Build navigation by root document
     *
     * @return array
     */
    public function buildNavigation() : array
    {
        // Root document for navigation builder
        if(Site::isSiteRequest()) {
            $rootDocument = Site::getCurrentSite()->getRootDocument();
        } else {
            $rootDocument = Document::getById(1);
        }

        return $this->getPageData($rootDocument);
    }

    /**
     * Get data for one page
     * [Along with it's sub pages]
     *
     * @param Pimcore\Model\Document\Page $page
     * @return array
     */
    protected function getPageData($page) : array
    {
        $title = $page->getProperty('navigation_title') ?: $page->getTitle();
        $path = $page->getFullPath();

        // "match" function trows exception
        // => wrap it in try/catch so it doesnt break code execution
        try {
            $route = $this->router->match($path)['_route'];
        } catch(\Exception $exception) {
            $route = null;
        }

        $isActive = $route == $this->currentRoute;

        $isRootPage = $page->getProperty('sidebar_root_page');
        $navigationIcon = $page->getProperty('sidebar_nav_icon');
        $excludeFromNavigation = $page->getProperty('navigation_exclude');

        $navigationData = [
            'title' => $title,
            'path' => $path,
            'active' => $isActive,
            'active_path' => false,
            'is_root' => $isRootPage,
            'navigation_icon' => $navigationIcon,
            'navigation_exclude' => $excludeFromNavigation,
            'sub_pages' => $this->getSubPages($page),
        ];

        return $navigationData;
    }

    /**
     * Get subpages and their data by page
     *
     * @param Pimcore\Model\Document\Page  $page
     * @return array
     */
    protected function getSubPages($page) : array
    {
        $subPagesData = [];
        $subPages = $page->getChildren();

        if ($subPages) {
            foreach($subPages as $subPage) {
                // Skip snippets
                if (!$subPage instanceof Page) {
                    continue;
                }

                $subPagesData[] = $this->getPageData($subPage);
            }
        }

        return $subPagesData;
    }

}