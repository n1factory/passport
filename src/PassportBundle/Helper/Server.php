<?php

namespace PassportBundle\Helper;

class Server {

    /**
     *
     * @return string
     */
    public static function getScheme() {
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol = 'https';
        } else {
            $protocol = 'http';
        }

        return $protocol;
    }

    /**
     *
     * @return string
     */
    public static function getBaseUrl ($url = null) {
        $protocol = self::getScheme();

        $baseUrl = "";

        if ($_SERVER['HTTP_HOST'] !== null) {
            $baseUrl = $protocol . "://" . filter_input(INPUT_SERVER, "HTTP_HOST");
        } else {
            $baseUrl = self::getDomainFromSystemSettings();
        }

        if($url !== null && is_string($url) === true) {
            if(substr($url, 0, 1) !== "/") {
                $url = "/" . $url;
            }

            $baseUrl .= $url;
        }

        return $baseUrl;
    }

    private static function getDomainFromSystemSettings() {
        $domain = \TrueRomanceBundle\Library\Config::get(["domain"]);

        if ($domain === null) {
            throw new \Exception("Domain-setting missing in system.php.");
        }

        return $domain;
    }

}
