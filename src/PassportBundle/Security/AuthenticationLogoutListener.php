<?php

namespace PassportBundle\Security;

use PassportBundle\Service\Sso\Session;
use Pimcore\Bundle\EcommerceFrameworkBundle\EnvironmentInterface;
use Pimcore\Bundle\EcommerceFrameworkBundle\Tools\SessionConfigurator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Cmf\Component\Routing\ChainRouterInterface;
use Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


class AuthenticationLogoutListener extends DefaultLogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    protected $environment;
    protected $sessionConfigurator;
    protected $session;
    private $sessionService;
    private $router;

    public function __construct(
        HttpUtils $httpUtils,
        string $targetUrl = '/',
        EnvironmentInterface $environment,
        SessionConfigurator $sessionConfigurator,
        SessionInterface $session,
        ChainRouterInterface $router,
        Session $sessionService
    ) {
        parent::__construct($httpUtils, $targetUrl);

        $this->environment = $environment;
        $this->sessionConfigurator = $sessionConfigurator;
        $this->session = $session;
        $this->sessionService = $sessionService;
        $this->router = $router;
    }

    /**
     * Creates a Response object to send upon a successful logout.
     *oces sneak peak na passport
     * @param Request $request
     *
     * @return Response never null
     */
    public function onLogoutSuccess(Request $request)
    {
        $this->sessionService->deleteDataByInternalSessionId($request->getSession()->getId());

        // unset user in environment
        $this->environment->setCurrentUserId(null);
        $this->environment->save();

        // clear complete e-commerce framework session
        $this->sessionConfigurator->clearSession($this->session);

        $targetLink = $this->router->generate('login', [], $this->router::NETWORK_PATH);

        return new RedirectResponse($targetLink);
    }
}
