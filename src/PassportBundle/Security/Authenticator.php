<?php

namespace PassportBundle\Security;

use PassportBundle\Service\ConfigService;
use PassportBundle\Service\Sso\Session;
use Pimcore\Tool;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Carbon\Carbon;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Cmf\Component\Routing\ChainRouterInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Authenticator extends AbstractGuardAuthenticator implements AuthenticationEntryPointInterface
{
    public const TARGET_PATH_NAME = '_target_path';

    private $config;
    private $translator;
    private $encoderFactory;
    private $hasError;
    private $router;
    private $ssoSession;
    private $session;
    private $security;

    public function __construct(
        TranslatorInterface $translator,
        EncoderFactoryInterface $encoderFactory,
        ConfigService $configService,
        ChainRouterInterface $router,
        Session $ssoSession,
        SessionInterface $session,
        Security $security
    )
    {
        $this->config = $configService->getConfiguration();
        $this->translator = $translator;
        $this->encoderFactory = $encoderFactory;
        $this->router = $router;
        $this->ssoSession = $ssoSession;
        $this->security = $security;
        $this->session = $session;
    }

    /**
     *
     * @return bool
     */
    public function getHasError()
    {
        return $this->hasError;
    }

    /**
     *
     * @param bool $hasError
     */
    public function setHasError($hasError)
    {
        $this->hasError = $hasError;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
        return ($request->get("_username") !== null && $request->get("_password")) || $request->get("_tok");
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        if($request->get("_username") && $request->get("_password")) {
            return array(
                'user' => $request->get("_username"),
                'password' => $request->get("_password"),
            );
        } else {
            return array(
                'token' => $request->get("_tok"),
            );
        }
    }

    public function getUser($credentials, UserProviderInterface $userProvider) {
        // if user is already logged - to not destroy sso login
        if($this->security && is_object($this->security->getUser())) {
            header("Location: /");
            exit;
        }

        $userListingEmail = \Pimcore\Model\DataObject\Customer::getByEmail($credentials["user"]);

        if ($userListingEmail->current() !== null && $userListingEmail->current() !== false) {
            return $userListingEmail->current();
        }

        throw new AuthenticationException($this->translator->trans('user.login.bad.credentials'));
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        if($credentials["token"])  {
            return true;
        }

        if($user->getActive() !== true) {
            return false;
        }

        if($user->getPassword() === "" || $user->getPassword() === null) {
            return $user->getZip() === $credentials["password"];
        }

        if($user->getPassword() !== "") {
            $userEncoder = $this->encoderFactory->getEncoder($user);
            /* @var $userEncoder \Pimcore\Security\Encoder\PasswordFieldEncoder */

            return $userEncoder->isPasswordValid($user->getPassword(), $credentials["password"], $user->getUsername());
        }

        return false;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $user = $token->getUser();
        $user->setLastLogin(Carbon::now())
            ->save();

        /**
         * SSO login (From other application sites, eg. Pricemaker, Match...)
         */
        if($request->get('system-id') && $request->get('token') && $request->get('checksum')) {
            if ($this->ssoSession->appIsAllowedForUser($request->get('system-id')) === false) {
                return new RedirectResponse("/app-not-allowed");
            }

            $this->ssoSession->createFromRequest($request);

            $this->ssoSession->createFromArray([
                'external_session' => 0,
                'internal_session' => $request->getSession()->getId(),
                'system' => 'usermanagement',
                'checksum' => $request->get("checksum") ?? null,
                'user' => $this->ssoSession->getUserData()
            ]);

            if($request->get('return')) {
                return new RedirectResponse($request->get('return'));
            }
        }

        $this->ssoSession->createFromArray([
            'external_session' => 0,
            'internal_session' => $request->getSession()->getId(),
            'system' => 'usermanagement',
            'checksum' => $request->get("checksum") ?? null,
            'user' => $this->ssoSession->getUserData()
        ]);

        $targetLink = $this->router->generate('passport-applications', [], $this->router::NETWORK_PATH);

        return new RedirectResponse($targetLink);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if (!$request->get("_tok")) {

            // Set error message to session
            $errorMessage = $this->translator->trans('user.login.bad.credentials');
            $this->session->set(Security::AUTHENTICATION_ERROR, $errorMessage);

            $this->setHasError(true);

            // Redirect to same url from where we got request
            $url = Tool::getHostUrl() . $request->server->get('REQUEST_URI');

            /*
            * This append &authenticated to route => throws 404
            if (!$request->query->has('authenticated')) {
                $url .= "&authenticated=0";
            }*/

            return new RedirectResponse($url);
        }
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->router->generate('login'));
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
