<?php

namespace PassportBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;
use Pimcore\Tool\Console;

class CustomerListener
{
    /**
     * @param ElementEventInterface $e
     * @throws \Exception
     */
    public function postAdd(ElementEventInterface $e)
    {
        $element = $e->getElement();

        if ($element instanceof DataObject\Customer) {
            $this->updateUserOnOtherApplications($element);
        }
    }

    /**
     * @param ElementEventInterface $e
     * @throws \Exception
     */
    public function postUpdate(ElementEventInterface $e)
    {
        $element = $e->getElement();

        if ($element instanceof DataObject\Customer) {
            $this->updateUserOnOtherApplications($element);
        }
    }

    private function updateUserOnOtherApplications(DataObject\Customer $customer)
    {
        $versions = $customer->getVersions();
        $nrOfVersions = count($versions);

        $currentVersion = isset($versions[$nrOfVersions - 1]) ? $versions[$nrOfVersions - 1] : null;
        $prevVersion = isset($versions[$nrOfVersions - 1]) ? $versions[$nrOfVersions - 2] : null;

        $latestAllowedApplications = [];
        $prevAllowedApplications = [];

        if ($currentVersion && $currentVersion->getData()) {
            $latestAllowedApplications = $currentVersion->getData()->getAllowedApplications();
        }

        if ($prevVersion && $prevVersion->getData()) {
            $prevAllowedApplications = $prevVersion->getData()->getAllowedApplications();
        }

        $latestAllowedApplicationsIds = array_map(function (DataObject\Application $application) {
            return $application->getId();
        }, $latestAllowedApplications);

        $prevAllowedApplicationsIds = array_map(function (DataObject\Application $application) {
            return $application->getId();
        }, $prevAllowedApplications);

        $diff1 = array_diff($latestAllowedApplicationsIds, $prevAllowedApplicationsIds);
        $diff2 = array_diff($prevAllowedApplicationsIds, $latestAllowedApplicationsIds);

        if (empty($diff1) && empty($diff2)) {
            return;
        }

        $command = 'api:update-users --user-id=' . $customer->getId();

        Console::runPhpScriptInBackground(
            realpath(PIMCORE_PROJECT_ROOT . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'console'),
            $command
        );
    }
}
