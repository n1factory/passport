<?php

namespace PassportBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;
use Pimcore\Tool\Console;

class RoleListener
{
    /**
     * @param ElementEventInterface $e
     * @throws \Exception
     */
    public function postAdd(ElementEventInterface $e)
    {
        $element = $e->getElement();

        if ($element instanceof DataObject\Role) {
            $element->setRoleId($this->generateRoleId($element));

            $element->save();
        }
    }

    /**
     * @param ElementEventInterface $e
     * @throws \Exception
     */
    public function postCopy(ElementEventInterface $e)
    {
        $element = $e->getElement();

        if ($element instanceof DataObject\Role) {
            $element->setRoleId($this->generateRoleId($element));

            $element->save();
        }
    }

    private function generateRoleId(DataObject\Role $role)
    {
        return uniqid() . '_' . $role->getId();
    }
}
