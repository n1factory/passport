<?php


namespace PassportBundle\EventListener;

use Pimcore\Model\DataObject\Customer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\Security\Core\Security;
use PassportBundle\Service\Sso\Session as SessionService;

class Session
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var SessionService
     */
    private $ssoSession;

    public function __construct(Security $security, SessionService $ssoSession)
    {
        $this->security = $security;
        $this->ssoSession = $ssoSession;
    }

    public function checkSessionValidity(ControllerEvent $event)
    {
        $request = $event->getRequest();

        $urlPath = $request->getBaseUrl() . $request->getPathInfo();

        $allowedUrls = ['/login', '/sso/user-info', '/sso/user-logout', '/api/sso/application-login', '/mobile-login', '/mobile-exit'];
        if ($request->attributes->get("_pimcore_context") === 'admin' || in_array($urlPath, $allowedUrls)) {
            return;
        }

        // logged in case
        if ($this->security->getUser() && $this->security->getUser() instanceof Customer) {
            if ($request->get('system-id') && $request->get('token') && $request->get('checksum')) {

                if ($this->ssoSession->appIsAllowedForUser($request->get('system-id')) === false) {
                    $event->setController(function () {
                        return new RedirectResponse("/app-not-allowed");
                    });

                    return;
                }

                $this->ssoSession->createFromRequest($request);

                $redirectUrl = $request->get('return');
                $event->setController(function () use ($redirectUrl) {
                    return new RedirectResponse($redirectUrl);
                });
            }
        }

        // not logged in case
        if (!$this->security->getUser() && $request->get('system-id') && $request->get('token') && $request->get('checksum')) {
            $urlParams = $request->query->all();

            if ($request->get('mobile_login')) {
                $event->setController(function () use ($urlParams) {
                    return new RedirectResponse('/mobile-login?' . http_build_query($urlParams));
                });
            } else {
                $event->setController(function () use ($urlParams) {
                    return new RedirectResponse('/login?' . http_build_query($urlParams));
                });
            }
        }
    }
}
