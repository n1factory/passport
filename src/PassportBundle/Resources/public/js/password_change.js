/**
 * Profile page password strength logic (PassportBundle)
 */
$(document).ready(function() {
    if ($('.content-container--passport #user-profile-page').exists()) {
        const $passwordInput = $('.content-container--passport #user-profile-page .password-form-row .new-password-input')
        const $passwordResultBar = $('.content-container--passport #user-profile-page .password-form-row .password-strength-container .result')

        checkPassword()
        $passwordInput.on('input', () => checkPassword())

        /**
         * Check strentgh of password
         */
        function checkPassword() {
            const passwordValue = $passwordInput.val()
            const $prerequisiteLables = $('.password-prerequisites .prerequisites-list')

            let strength = 0
            let color = '#9F1505'

            const passwordPatterns = {
                symbols: /^.{8,}$/,
                number: /([0-9])/,
                uppercase: /([A-Z])/,
                lowercase: /([a-z])/,
                special_char: /[#\?\!@$%\^&*-\+\'\.§\{\}\[\]=\`°\^-_:;,]/,
            }

            // Check trough pattern matches, count strength for bar & set labels
            for (patternKey in passwordPatterns) {
                const pattern = passwordPatterns[patternKey]
                const $label = $prerequisiteLables.find(`[data-type="${patternKey}"]`)

                if (passwordValue.match(pattern)) {
                    strength += 1

                    $label.addClass('filled')
                } else {
                    $label.removeClass('filled')
                }
            }

            // Set color for bar via strength value
            if (strength < 3) {
                color = '#9F1505'
            } else if (strength == 3) {
                color = '#E0D00D'
            } else {
                color = '#548B16'
            }

            $passwordResultBar.css({
                'width': `${(strength/5) * 100}%`,
                'background': color
            })
        }
    }
})
