/**
 * Profile page logic (PassportBundle)
 */
$(document).ready(function() {
    if ($('.content-container--passport #user-profile-page').exists()) {
        const $pageContainer = $('.content-container--passport #user-profile-page .user-profile-page-container')
        const $editButton = $pageContainer.find('.start-edit-button')
        const $cancelButton = $pageContainer.find('.cancel-button')
        const $passwordToggleButton = $pageContainer.find('.toggle-password-visibility')
        const $formSubmitButton = $pageContainer.find('.save-button')

        // Selects init
        const $formSelects = $pageContainer.find('.content-box select')
        $formSelects.each(function() {
            const $select = $(this)
            $select.select2({
                minimumResultsForSearch: -1,
                dropdownParent: $select.closest('.form-group')
            })
        })

        /**
         * Toggle password visibility on password input field
         */
        $passwordToggleButton.on('click touch', function() {
            const $button = $(this)
            const $input = $button.closest('.form-group').find('input')

            if($button.hasClass('visible')) {
                $button.removeClass('visible')
                $input.attr('type', 'password')
            } else {
                $button.addClass('visible')
                $input.attr('type', 'text')
            }
        })

        /**
         * Enable user data edit
         */
        $editButton.on('click', function() {
            const $button = $(this)
            const $contentBox = $button.closest('.content-box')

            $contentBox.removeClass('preview')
            $contentBox.removeClass('success')
            $contentBox.find('.form-group select,input').prop('disabled', false)
        })

        /**
         * Disable user data edit and discard changes
         */
        $cancelButton.on('click', function() {
            const $button = $(this)
            const $contentBox = $button.closest('.content-box')

            $contentBox.addClass('preview')
            $contentBox.find('.form-group select,input').prop('disabled', true)
        })

        // Submit action
        $formSubmitButton.on('click', function() {
            if ($(this).hasClass('is-Requesting')) {
                return
            }

            const $contentBox = $(this).closest('.content-box')
            const $form = $contentBox.find('form')

            // Clean form validations
            cleanFormValidations($form)

            handleFormSubmit($(this))
        })

        /**
         * Form submit ajax handler
         * @param {*jQuery Object} $button
         */
        async function handleFormSubmit($button) {
            $button.addClass('is-Requesting')
            const $contentBox = $button.closest('.content-box')
            const $form = $contentBox.find('form')

            // Submit route
            const route = $form.attr('action')

            // Create formData for submission
            const payload = createPayloadFromForm($form)

            try {
                // Make request
                const response = await axios.post(route, payload)

                // Remove spinner
                $button.removeClass('is-Requesting')

                if (response.data.redirect_link) {
                    window.location.href = response.data.redirect_link
                }

                // Update content box
                $contentBox.addClass('success')
                $contentBox.addClass('preview')
            } catch (err) {
                $button.removeClass('is-Requesting')

                if (err.response.status == 400) {
                    // Handle validation errors
                    const { data } = err.response.data

                    handleFormErrors($form, data)
                } else {
                    // Other error response status
                    console.error(err)
                    window.showErrorPopup()
                }

                throw err
            }
        }

        /**
         * Create FormData for ajax call
         */
        function createPayloadFromForm($form) {
            const formData = new FormData()

            // Form inputs [along with hidden csrf _token]
            const inputs = $form.find('input,select')
            inputs.each(function() {
                const $input = $(this)

                const inputName = $input.attr('name')
                const inputValue = $input.val()

                formData.append(inputName, inputValue)
            })

            return formData
        }

        /**
         * Clean validations classes & error messages
         */
        function cleanFormValidations($form) {
            const $formGroups = $form.find('.form-group.has-error')
            $formGroups.each(function() {
                $(this).removeClass('has-error')

                const $errMessageContainer = $(this).find('.error-message')
                if($errMessageContainer.exists()) {
                    $errMessageContainer.html('')
                }
            })
        }

        /**
         * Handle form errors returned from API
         */
        function handleFormErrors($form, { errors }) {
            if (!errors) return

            if(errors.hasOwnProperty('csrf_token')) {
                const csrfError = errors['csrf_token'][0]

                window.showErrorPopup(__(csrfError), null, () => {
                    location.reload()
                })

                return
            }

            Object.keys(errors).forEach(fieldName => {
                const inputSelector = `input[name="${fieldName}"], select[name="${fieldName}"], textarea[name="${fieldName}"]`
                const $input = $form.find(inputSelector)

                if($input.exists()) {
                    const $inputContainer = $input.closest('.form-group')
                    const $errMessageContainer = $inputContainer.find('.error-message')
                    const fieldErrors = errors[fieldName]

                    /**
                     * @Todo handle more error messages for one field
                     */
                    if (fieldErrors.length > 1) {
                        const errMessage = fieldErrors[0]
                        $errMessageContainer.html(errMessage)
                    } else if (fieldErrors[0]) {
                        const errMessage = fieldErrors[0]
                        $errMessageContainer.html(errMessage)
                    }

                    $inputContainer.addClass('has-error')
                }
            })
        }
    }
})