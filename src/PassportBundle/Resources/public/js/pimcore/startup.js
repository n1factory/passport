pimcore.registerNS("pimcore.plugin.PassportBundle");

pimcore.plugin.PassportBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.PassportBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("PassportBundle ready!");
    }
});

var PassportBundlePlugin = new pimcore.plugin.PassportBundle();
