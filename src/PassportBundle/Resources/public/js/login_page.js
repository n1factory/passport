/**
 * PassportBundle login page logic
 */
$(document).ready(() => {
    if($('#passport-login-page').exists()) {

        /**
         * Toggle password visibility on password input field
         */
        $('#passport-login-page .login-form .form-group .toggle-password-visibility').on('click touch', function() {
            const $button = $(this)
            const $input = $button.closest('.form-group').find('input.form-control')

            if($button.hasClass('visible')) {
                $button.removeClass('visible')
                $input.attr('type', 'password')
            } else {
                $button.addClass('visible')
                $input.attr('type', 'text')
            }
        })

        $('#passport-login-page #login-form').on('submit', function() {
            $(this).find('#_submit').addClass('is-Requesting')
        })

        $('#passport-login-page #_username').trigger('focus')
    }
})
