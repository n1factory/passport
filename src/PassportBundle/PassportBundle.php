<?php

namespace PassportBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class PassportBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/passport/js/pimcore/startup.js'
        ];
    }
}