<?php

namespace  PassportBundle\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Log\Simple;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use PassportBundle\Service\UserService;

class ApiUserCommand extends AbstractCommand
{
    protected static $defaultName = 'api:update-users';

    /** @var UserService $userService */
    private $userService;

    public function __construct(UserService $userService, string $name = null)
    {
        $this->userService = $userService;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setDescription(
            'Update user on every site. If user-id is sent then only user with that id will be updated.')
            ->addOption(
                'user-id',
                null,
                InputOption::VALUE_REQUIRED,
                'ID of user that has to be updated.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userId = $input->getOption('user-id');

        try {
            if ($userId) {
                $this->userService->updateUserById(intval($userId));
            }
        } catch (\Throwable $e) {
            Simple::log('user_update', $e->getMessage());
        }

        return 1;
    }
}
