<?php

namespace  PassportBundle\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Model\DataObject\Customer;
use Pimcore\Model\DataObject\Fieldcollection;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateAllowedApplicationsCommand extends AbstractCommand
{
    protected static $defaultName = 'update:allowed-applications';

    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setDescription(
            'Temporary command for updating allowed applications, once this is ran it can be removed.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $customers = Customer::getList()->setUnpublished(true)->getObjects();

        /** @var Customer $customer */
        foreach ($customers as $customer) {
            $allowedApplications = $customer->getAllowedApplications() ?: [];

            $userApplicationsFD = new Fieldcollection();

            foreach ($allowedApplications as $allowedApplication) {
                $userApplication = new Fieldcollection\Data\AllowedApplication();

                $userApplication->setApplication($allowedApplication);
                $userApplicationsFD->add($userApplication);
            }

            $customer->setUserApplications($userApplicationsFD)
                ->save();
        }

        return 1;
    }
}
