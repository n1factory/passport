<?php

require_once __DIR__ . '/../../startup.php';

ini_set("memory_limit", "8192M");

use TrClassUpdateBundle\Library\Cli\Helper as CliHelper;
use ToolsBundle\Data\CSV;
use Pimcore\Model\DataObject;

$foldersToDelete = [
    "/Contracts",
    "/Deliverynotes",
    "/Invoices",
    "/ProductHierarchy",
    "/Recipes",
    "/Suppliers",
    "/TBSoftCustomers",
    "/VehicleTypes",
    "/Materials"
];

foreach ($foldersToDelete as $folderToDelete) {
    $folder = DataObject\Folder::getByPath($folderToDelete);

    if(!$folder) {
        continue;
    }

    $folderChildCount = $folder->getChildAmount();

    CliHelper::success("Delete {$folderChildCount} childs");

    foreach ($folder->getChildren() as $index => $child) {
        $count = $index + 1;

        $child->delete();

        CliHelper::success("{$count}: Delete child ");
    }

    $folder->delete();

    CliHelper::success("Delete folder: {$folderToDelete}");
}

