<?php

require_once __DIR__ . '/../../startup.php';

error_reporting(E_ALL);

ini_set("memory_limit", "8192M");

use TrClassUpdateBundle\Library\Cli\Helper as CliHelper;
use ToolsBundle\Data\CSV;
use Pimcore\Model\DataObject;



$classesToDelete = [
    "Contract",
    "ContractItem",
    "FilterDefinition",
    "LinkActivityDefinition",
    "OfferToolCustomProduct",
    "OfferToolOffer",
    "OfferToolOfferItem",
    "OnlineShopOrder",
    "OnlineShopOrderItem",
    "OnlineShopTaxClass",
    "OnlineShopVoucherSeries",
    "OnlineShopVoucherToken",
    "Product",
    "ProductAttribute",
    "ProductCategory",
    "ProductCollection",
    "SsoIdentity",
    "TermSegmentBuilderDefinition",
    "ProductTechnology",
    "CallOff",
    "CallOffType",
    "CallOffUpdate",
    "DailyPlan",
    "Deliverynote",
    "Invoice",
    "Material",
    "ProductHierarchy",
    "Recipe",
    "RecipeItem",
    "VehicleType",
    "Incoterm",
    "Disposition",
    "N1RoleTest",
    "VKOrganisation"
];

foreach ($classesToDelete as $class) {
    $classInstance = DataObject\ClassDefinition::getByName($class);

    if($classInstance) {
        CliHelper::success("Delete class {$class}");

        $classInstance->delete();
    }
}
