<?php

require_once __DIR__ . '/../../startup.php';

error_reporting(E_ALL);

ini_set("memory_limit", "8192M");

use TrueRomanceBundle\Library\Scripts\CleanSetup;

$cleanSetupObject = new CleanSetup();

$cleanSetupObject->deleteCustomers();

$cleanSetupObject->createNewUsers();
