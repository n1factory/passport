<?php

require_once __DIR__ . '/../../startup.php';

ini_set("memory_limit", "8192M");

$listingClasses = [
    'PermissionItem' => new \Pimcore\Model\DataObject\PermissionItem\Listing(),
    'InitialToken' => new \Pimcore\Model\DataObject\InitialToken\Listing(),
    'Invitation' => new \Pimcore\Model\DataObject\Invitation\Listing()
];

foreach ($listingClasses as $className => $listingClass) {
    $count = count($listingClass->getObjects());

    foreach ($listingClass->getObjects() as $index => $object) {
        ++$index;

        \TrClassUpdateBundle\Library\Cli\Helper::success("Deleted object of class $className ($index of $count)");
        $object->delete();
    }
}

