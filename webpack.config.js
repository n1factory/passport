const VueLoaderPlugin = require('vue-loader/lib/plugin')
module.exports = {
    output: {
      filename: 'bundle.js',
    },
    // resolve: {
    //   alias: {
    //     vue: 'vue/dist/vue.min.js'
    //   }
    // },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader'
        },
        {
          test: /\.css$/,  
          include: /node_modules/,  
          loaders: ['style-loader', 'css-loader'],
        },
        {
          test: /\.sass$/,
          use: [
            'vue-style-loader',
            'css-loader',
            {
              loader: 'sass-loader',
              options: {
                indentedSyntax: true,
                // sass-loader version >= 8
                sassOptions: {
                  indentedSyntax: true
                }
              }
            }
          ]
        },
        
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          exclude: file => (
            /node_modules/.test(file) &&
            !/\.vue\.js/.test(file)
          ),
          query: {
            presets: ["@babel/env"],
            plugins: [
                "@babel/plugin-transform-runtime",
                "@babel/plugin-transform-async-to-generator"
            ]
          },
        },
      ],
    },
    plugins: [
      // make sure to include the plugin!
      new VueLoaderPlugin()
    ]
};